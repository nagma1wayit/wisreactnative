/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */ 


import React, {Component} from 'react';
import { View, Text, TouchableOpacity, TextInput,ActivityIndicator,AsyncStorage, ImageBackground, Dimensions, StyleSheet, Image} from 'react-native'
import homes from './homes';
import stacknav from './stacknav';
import shareholderHome from './shareholderHome';
import  referFriend from './referFriend';
import  notifications from './notifications';
import  mileage from './mileage';
import  invoices from './invoices';
import  expenses from './expenses';
import  contactUs from './contactUs';
import  aboutUs from './aboutUs';
import newinvoice from './newinvoice';
import selectcustomer from './selectcustomer';
import items from './items';
import expenseitem from './expenseitem';
var scr = "Home"
const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}   
 
import login from './login';
import accmodation from './accmodation';
import addexpitem from './addexpitem';
import addmileageitems from './addmileageitems';
import newexpense from './newexpense';
import newmileage from './newmileage';
import selectempexp from './selectempexp';
import selectmonthexp from './selectmonthexp';
import selectyearexp from './selectyearexp';
import selectyearmileage from './selectyearmileage';
import selectmonthmileage from './selectmonthmileage';
import selectempmileage from './selectempmileage';
import invoicedetail from './invoicedetail';
import invoiceitems from './invoiceitems';
import expensedetail from './expensedetail';

import { createStackNavigator, createDrawerNavigator, createAppContainer} from 'react-navigation';


const Menudrawer = createDrawerNavigator({
companyscreen: {screen:homes},
stack: {screen:stacknav},
shares:{screen:shareholderHome},
refer:{screen:referFriend},
notification:{screen:notifications},
milen:{screen:mileage},
expen:{screen:expenses},
cont:{screen:contactUs},
abt:{screen:aboutUs},
invoice:{screen:invoices},
},
{ 
  
  contentComponent: stacknav,
  drawerWidth:dimensions.fullWidth-75 ,
  drawerPosition : 'left',
   drawerOpenRoute: 'DrawerRightOpen',
    drawerCloseRoute: 'DrawerRightClose',
    drawerToggleRoute: 'DrawerRightToggle',
});
 
  
   
    const RootStacks = createStackNavigator(
      {
        Home: {
          screen:login
        },
        companyscreen:{screen:Menudrawer}, 
        newinvoices:{screen:newinvoice},
        selectcustom:{screen:selectcustomer},
        additem:{screen:items},
        addmileage:{screen:addmileageitems},
        addexp:{screen:addexpitem},
        newexp:{screen:newexpense},
        empexp:{screen:selectempexp},
        monthexp:{screen:selectmonthexp},
        yearexp:{screen:selectyearexp},
        monthmileage:{screen:selectmonthmileage},
        yearmileage:{screen:selectyearmileage},
        empmileage:{screen:selectempmileage},
        newmileages:{screen:newmileage},
        accm:{screen:accmodation},
        invoicedetails:{screen:invoicedetail},
        invoiceitem:{screen:invoiceitems},
        expenseDetail:{screen:expensedetail},
        expitem:{screen:expenseitem},
        
      },
      {initialRouteName: "Home",
      headerMode: 'none' 
    },
      
      
    );
    const RootStack1 = createStackNavigator(
      {
        Home: {
          screen:login
        },
        companyscreen:{screen:Menudrawer},
       
        newinvoices:{screen:newinvoice},
        selectcustom:{screen:selectcustomer},
        additem:{screen:items},
        addmileage:{screen:addmileageitems},
        addexp:{screen:addexpitem},
        newexp:{screen:newexpense},
        empexp:{screen:selectempexp},
        monthexp:{screen:selectmonthexp},
        yearexp:{screen:selectyearexp},
        monthmileage:{screen:selectmonthmileage},
        yearmileage:{screen:selectyearmileage},
        empmileage:{screen:selectempmileage},
        newmileages:{screen:newmileage},
        accm:{screen:accmodation},
        invoicedetails:{screen:invoicedetail},
        invoiceitem:{screen:invoiceitems},
        expenseDetail:{screen:expensedetail},
        expitem:{screen:expenseitem},

      },
      {initialRouteName: "companyscreen",
       headerMode: 'none'},
       
      
    );


    

    
    const Apps = createAppContainer(RootStacks);
    const App1 = createAppContainer(RootStack1);

    export default class App extends Component{
     
      constructor(props){
        super(props);
        this.state = { 
             
            uid:'',
            scr : "Home",
            isLoading: true,
            
    
            }
          }
      componentDidMount(){
           
    
        AsyncStorage.getItem("uid").then((value) => {
          console.log({"uid : ": value});
          
            if (value == null ){
              scr = "Home"
              this.setState({uid:value ,isLoading:false});
            } else{
              scr = "companyscreen"
              this.setState({uid:value ,isLoading:false});
            }
        
            
           
        }).done();
      }
     
    
    
      render() {
        if (this.state.isLoading) {
           
          return (
            <View style={{ flex: 1, paddingTop: 0,justifyContent:"center",alignSelf:"center" }}>
              <View style={{backgroundColor:"#F3F3F7",borderRadius:10,height:70,width:70,justifyContent:"center",alignItems:"center"}}><ActivityIndicator size="large" color="grey"/></View>
            </View>
          );
        } 
        if(this.state.uid == null){ 
        return(<Apps/>)
      } 
      else
      {
        return (
          <App1/>
        );
      }
        
      }
    }
    
    