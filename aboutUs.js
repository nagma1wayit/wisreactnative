import React, { Component } from 'react';
import { View, Text,SafeAreaView,ScrollView ,Dimensions,TouchableOpacity,Image} from 'react-native';
const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }
export default class selectcustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  toggleDrawer1 = () => {
 
    console.log(this.props.navigation);
 
    this.props.navigation.toggleDrawer();
 
  }


  render() {
    return (
      <SafeAreaView>
        <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"#EBEBF1",marginTop:0}}>
      <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
       <TouchableOpacity onPress={this.toggleDrawer1}  style={{width:50, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
       <Image source={require("./Images/menu.png")}  style={{height:20,width:20}}/>
       </TouchableOpacity>
       <Text style={{ width:dimensions.fullWidth-125,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>ABOUT US</Text>
       </View> 
       <ScrollView>
        <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:1000}}>
        <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:90,justifyContent:"center",alignSelf:"center"}}>
        <Image source={require("./Images/newlogo.png")}  style={{height:80,width:100,marginLeft:15}}/></View>
         <Text style={{width:dimensions.fullWidth-20,marginLeft:10,textAlign:"justify"}}>Wis Accountancy is a specialised contractor accountant with extensive experience in handling limited company contractors' finances and unique mortgage/life insurance needs. Unlike most accountancy firms which tend to be overly corporate and often distant with clients, we will provide you exactly what you want by developing a friendly and personal relationship. With the assistance of a dedicated relationship manager and our contractor friendly online cloud based software you wouldn't have any issue managing your limited company. We are regulated be Chartered Management Accountants (CIMA) and are members of IPSE.</Text> 
        <Text style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:15,textAlign:"justify"}}>       Not only do we advise on the most tax-efficent ways of running your limited company, we now provide comprehensive advice on Contractor specific mortgages and specialist tax-allowable insurance policies tailored to Contractors.</Text>
        <Text style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,fontSize:15,color:"black",fontWeight:"bold"}}>Summary of Services</Text>

       <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Free Company formation.</Text>
        </View>
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Free Business Banking.</Text>
        </View>
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Free access to your online cloud simple to use portal.</Text>
        </View>
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Free unlimited IR35 contract reviews.</Text>
        </View>
        
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Free Reference letters for Visa's and Mortgages.</Text>
        </View>
        
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Discount on PI and PL insurances.</Text>
        </View>
        
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Monthly RTI Payroll (PAYE) submission.</Text>
        </View>
       
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Quarterly VAT prepartion and submission.</Text>
        </View>
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Final Accounts and Corporation Tax and submission.</Text>
        </View>
        
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Personal Self-Assesment tax return prepartion and submission.</Text>
        </View>
        
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Ability to use our address as Registered address</Text>
        </View>
        
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Assistance with securing a Mortgage.</Text>
        </View>
        
        <View style={{width:dimensions.fullWidth-20,marginLeft:10,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Text style={{fontSize:15,fontWeight:"bold"}}>{'\u2022'}</Text>
        <Text style={{marginLeft:8}}>Assistance with securing Tax-free Insurance covers via your limited company</Text>
        </View>
        
       
        </View>
        </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
