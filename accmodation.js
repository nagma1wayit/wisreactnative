import React, { Component } from 'react'
import { Text, View,SafeAreaView,AsyncStorage,navigation,TouchableOpacity,Image,ScrollView,StyleSheet,Dimensions,FlatList} from 'react-native'
import { SwipeRow, Icon, Button } from 'native-base';
 
const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }
  export default class newmileage extends Component {
    constructor(props) {
      super(props);
      this.state = {
       refreshing: false,
       apk:'',
       data1:[],
         
      };
      
    }
    componentDidMount(){


   
      AsyncStorage.getItem("apk").then((value) => {
        
        this.setState({apk: value});
        console.log(this.state.apk)
        this.acmlist()
      
    }).done();
     }
     acmlist (){

      fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/expenseCategoryList/' + this.state.apk )
         .then((response) => response.json())
         .then((responseJson) => {
    
    
           console.log("accmo list name:",responseJson[0].Categories)
           
           this.setState({
            
            data1: responseJson[0].Categories
           })
           
           
         })
         .catch((error) =>{
           console.log(error);
         });
       }

    gobackAction=()=>{
       
        this.props.navigation.goBack()
            
             
        }
        empty = () => {
          return (
            <View style={{height:50,width:dimensions.fullWidth,backgroundColor:"#EFEFF4"}}/>
          );
        };

       
        getItem(acm,accmid){
             //console.log("idal",accmlistid)
  

            AsyncStorage.setItem('accmo', acm);
            this.setState({accmo : acm});
            
            AsyncStorage.setItem('acmid', accmid);
            this.setState({acmid : accmid});
            
            this.props.navigation.navigate('addexp', {
                onGoBack: () => this.refresh()})
            
         
    }
    refresh(){


    }

  render() {
    
    return (
        <SafeAreaView  style={{backgroundColor:"white",flex:1}}>
        <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"white",marginTop:0}}>
       <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
        <TouchableOpacity onPress = {this.gobackAction}  style={{ width:40, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
        <Image source={require("./Images/leftarrow.png")}  style={{height:20,width:25,tintColor:"white"}}/>
        </TouchableOpacity>
        <Text style={{width:dimensions.fullWidth-80,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>ACCAMODATION</Text>
        </View>
        <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:dimensions.fullHeight-80}}>
        
    <FlatList 
  
   ListFooterComponent={this.empty()}
  data={this.state.data1}
  ItemSeparatorComponent={this.renderSeparator}
  renderItem={({item}) => <View style={styles.flatview}>
       <TouchableOpacity onPress={this.getItem.bind(this,item.title,item.id)} style={{backgroundColor:"white",width:(dimensions.fullWidth-10),height:50,flexDirection:"row",alignItems:"center"}}>

       <Text  style={{backgroundColor:"white",fontSize:16,fontWeight:"bold",color:"black"}}>{item.title}</Text>

  
  
       
       </TouchableOpacity>
  </View>}
  
  
/>
</View>
        </View>
        </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  
  
    flatview: {
       height:60,
      justifyContent: 'center',
      paddingTop: 0,
      borderRadius: 1,
      alignItems:"center",
      flexDirection:"row",
      backgroundColor:"#EFEFF4"
    },
     
     
     
    item: {
      padding: 10,
      fontSize: 20,
      height: 45,
      backgroundColor:"red"
    }
  });
