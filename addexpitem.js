import React, { Component } from 'react'
import { Text,Modal,AsyncStorage,Alert,TouchableHighlight, View,SafeAreaView,ScrollView,TextInput,TouchableOpacity,Image,StyleSheet,Dimensions,FlatList} from 'react-native'
import { SwipeRow, Icon, Button } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import ImagePicker from 'react-native-image-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import {NavigationEvents} from 'react-navigation';

const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }
  export default class newmileage extends Component {
    constructor(props) {
      super(props);
      this.state = {
        userphoto: null,
        modalVisible: false,
        status:false,
        dateset:"Select Date",
        accmo:"Select Accommodation",
        isDateTimePickerVisible: false,
        refreshing: false,
        amount:'',
        clivat:'',
        acmid:'',
        datarecve:"",
        ENDEX:"",
        indexs:"", 
        indexid:"",     
        array:[],
        items:[],
        inputdesc:'',
        dateformchange:'',
        hoursrate:"",
        totals:"0.00",
        
      }
      this.component = [];
      this.selectedRow;
    }
    
    gobackAction=()=>{

       this.props.navigation.goBack()
      //  this.state.array.push({"myitem" : "back"})

      //  this.props.navigation.navigate('newexp', {"myObj" : this.state.array})
   
           
             
        }
        empty = () => {
          return (
            <View style={{height:(dimensions.fullHeight)/5-60,width:dimensions.fullWidth,backgroundColor:"pink"}}/>
          );
        };
        _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
 
        _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

        _handleDatePicked = (date) => {
 // console.log('A date has been picked: ', date);
        var dateObj = new Date(date);
        var momentObj = moment(dateObj);
        var momentString = momentObj.format("DD-MM-YYYY");
        var dateformat= momentObj.format("YYYY-MM-DD");
        this.setState({dateset:momentString});
        this.setState({dateformchange:dateformat})
        this._hideDateTimePicker();
};
componentWillReceiveProps(acc,acid) {

  AsyncStorage.getItem("accmo").then((value) => {
    
    this.setState({accmo: value});
    
   
 }).done();
 AsyncStorage.getItem("acmid").then((val) => {
    
  this.setState({acmid: val});
  
 
}).done();

  
}

componentDidMount() {

AsyncStorage.setItem('accmo', "Select Accamodation");
AsyncStorage.getItem("clivat").then((value) => { 
  this.setState({clivat: value});
  
 
}).done();
 this.EVEN()

}
refresh(){


}
EVEN(){
    
  if(this.props.navigation.state.params){
    console.log("rasta1",this.props.navigation.state.params)
    console.log("rasta",this.props.navigation.state.params.Senditem )
    console.log("rasta",this.props.navigation.state.params.Endex)
      if(this.props.navigation.state.params.Senditem && this.props.navigation.state.params.Endex+1){ 
  
      this.setState({datarecve:this.props.navigation.state.params.Senditem })
  
      this.setState({ENDEX:this.props.navigation.state.params.Endex+1})
     // this.setState({remainitem:this.props.navigation.state.params.reitems})
       const { navigation } = this.props;
  
       const recevs = navigation.getParam('Senditem');
       const recevsindex = navigation.getParam('Endex');
       const listitem = navigation.getParam('Reitems');
         this.setState({items:listitem})
         
         this.setState({indexs:recevsindex})
         this.setState({acmid: recevs.catId});
         var dateformat=recevs.ItemDate
        //  var changeformatset = new Date(dateformat);
        //  var changedateset = moment(changeformatset);
        //  var formchange=changedateset.format("DD-MM-YYYY");
        // console.log("anti ji golo",formchange)
           this.setState({dateset:dateformat})
           this.setState({accmo:recevs.catTitle})
          this.setState({inputdesc:recevs.Description})
         
          this.setState({amount:recevs.Amount})
          this.setState({indexid:recevs.ID})
        //  var drafttotal=recevs.Quantity * recevs.UnitPrice
        //  console.log("drfation",drafttotal)
        //  this.setState({totals:drafttotal})
        //  var pertotal = (drafttotal * recevs.Tax)/100
        //  var Amt = drafttotal + pertotal
        //  console.log("poi",Amt)
        //  this.setState({vatper:recevs.Tax})
        //  this.setState({Allamount:Amt})
         
          
      
         

    } 
 }
  }
handleChoosePhoto = () =>{
  
  ImagePicker.showImagePicker({ title:'Select a reciept', maxWidth: 800, maxHeight: 600}, res => {
 
    console.log('Response = ', res);

    if (res.didCancel) {
      console.log('User cancelled photo picker');
    }
    else if (res.error) {
      console.log('Image Error: ', res.error);
    }
    else if (res.customButton) {
      console.log('User tapped custom button: ', res.customButton);
    }
    else {
       
      this.setState({
        userphoto: { uri: res.uri }
        
      }); 
     
     
    }
  });
}
setModalVisible(visible) {
  this.setState({modalVisible: visible});
}
  delpik(use){
     
  
    {Alert.alert('Delete Reciept',
    'Are you sure you want to delete reciept ?',
    [
      {
        text: 'No',
       
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Yes',
       
        onPress: () =>{ 
          console.log("photo",use)
           
             var up = this.state.use; // make a separate copy of the array
             up =  null; 
             this.setState({userphoto:up})
          
      }
        
      },
    ],)}
  }
   
   
  backAction=(amnt,listid,desc,accmoname,seldate)=>{ 
     
  if(seldate == 'Select Date'){
    alert("Please Select Date ")
    }
    else if(accmoname=='Select Accommodation'){
      alert("Please Select Accommodation ")
         }
    else if(desc==0){
 alert("Please Enter Description")
    } 
else if(amnt== 0){
  alert("Please Enter Amount")
} 
    else{
      
      console.log("ok")
      
      this.state.array.push({"myitem" : {"Description":desc ,"catTitle":accmoname, "ItemDate":this.state.dateformchange , "Amount":amnt ,"catId":listid,"itemid":1,"ID":null}})
      console.log("objectdd",this.state.array)
      this.props.navigation.navigate('newexp', {"myObj" : this.state.array})

   //this.props.navigation.navigate('newexp', {"myitem" : {"textdesc":desc ,"acmname":accmoname, "setdate":seldate , "addexpamt":amnt ,"Acid":listid}})
    
}    
  
}
backActupdate=(amnt,listid,desc,accmoname,seldate,indexr,listdata,idget)=>{ 
  
if(seldate == 'Select Date'){
 alert("Please Select Date ")
 }
 else if(accmoname==0){
   alert("Please Select Accammodation ")
      }
 else if(desc==0){
alert("Please Enter Description")
 } 
else if(amnt== 0){
alert("Please Enter Amount")
} 
 else{
   
  var arrays ={"Description":desc ,"catTitle":accmoname, "ItemDate":this.state.dateformchange , "Amount":amnt ,"catId":listid,"itemid":indexr,"ID":idget}
  //   this.props.navigation.navigate({"myObj" : array},{
  //  onGoBack: () => this.refresh()})
   this.props.navigation.navigate('newexp' ,{"myObj":arrays})
   
  //  this.state.array.push({"myitem" : {"Description":desc ,"catTitle":accmoname, "ItemDate":seldate , "Amount":amnt ,"catId":listid,"itemid":indexr,"ID":idget}})
  //  console.log("objectdd",this.state.array)
  //  this.props.navigation.navigate('newexp', {"myObj" : this.state.array})

//this.props.navigation.navigate('newexp', {"myitem" : {"textdesc":desc ,"acmname":accmoname, "setdate":seldate , "addexpamt":amnt ,"Acid":listid}})
 
}    

}
myvalue(text,AMNT){
 
var totalamnt = parseInt(AMNT) + parseInt(text)  
 this.setState({amount:totalamnt})
}
 
 getvalue = (allvals) => { 

  if(allvals == 0 || allvals == null){
     
     return "0.00"
     
  }else{ 

    var value = 0;
    value = parseFloat(allvals).toFixed(2);

  return value

  } 
   
}

 myValue = (text) => {
  console.log("nik",text)
  this.setState({hoursrate:text}) 
 // this.setState({totals:text})
  this.datatotal(text)
   
}
datatotal=(text)=>{
   if(this.state.amount == null || this.state.amount == 0){
    var hrs="0"
    console.log("hrs",hrs)
    var totalval=  parseInt(hrs) + parseInt(text) 
    this.setState({totals:totalval})
   }else{
    var hrs=this.state.amount
    console.log("hrs",hrs)
    var totalval=  parseInt(hrs) + parseInt(text) 
    this.setState({totals:totalval})
   }
  
 }
 myValue1 = (text) => {
       
  this.setState({amount:text})
//  this.setState({totals:text})

  this.datatotal1(text)
}
datatotal1=(text)=>{
  console.log("pik",text)
  if(this.state.hoursrate == null || this.state.hoursrate == 0){
    var hrsrte="0"
    var totalval=  parseInt(hrsrte) + parseInt(text) 
    this.setState({totals:totalval})
   }else{
    var hrsrte=this.state.hoursrate
  var totalval=  parseInt(hrsrte) + parseInt(text) 
  this.setState({totals:totalval})
   }
 
 }
  render() {
   // var dlist =[{value: 'Bike'},{value: 'Bicycle'},{value: 'Car'},]
     console.log("viewtype",this.state.clivat)
    return (
      <SafeAreaView>
      <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"white",marginTop:0}}>
    <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
     <TouchableOpacity onPress={()=>this.gobackAction()} style={{width:40, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
     <Image source={require("./Images/leftarrow.png")}  style={{height:20,width:25,tintColor:"white"}}/>
     </TouchableOpacity>
     <Text style={{ width:dimensions.fullWidth-120,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>ADD ITEM</Text>
     {this.state.datarecve && this.state.ENDEX ? <TouchableOpacity onPress={() => this.backActupdate(this.state.amount,this.state.acmid,this.state.inputdesc,this.state.accmo,this.state.dateset,this.state.indexs,this.state.items,this.state.indexid)} style={{width:60,height:30,top:18,marginLeft:5,justifyContent:"center",alignItems:"center"}}><Text style={{color:"white",fontSize:16,fontWeight:"normal",}}>Update</Text></TouchableOpacity>:<TouchableOpacity onPress={() => this.backAction(this.state.amount,this.state.acmid,this.state.inputdesc,this.state.accmo,this.state.dateset)} style={{ width:40,height:30,top:18,marginLeft:20,justifyContent:"center",alignItems:"center"}}><Text style={{color:"white",fontSize:16,fontWeight:"normal",}}>Save</Text></TouchableOpacity>}
     </View>
 

     {this.state.clivat == 'flat' ? <View style={{width:dimensions.fullWidth,height:(dimensions.fullHeight-80),backgroundColor:"#EFEFF4"}}>
<ScrollView>
  {/* #F3F3F7 */}
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{width:(dimensions.fullWidth-10)/2-20,fontSize:15,paddingLeft:5,fontWeight:"bold",color:"#9A9A9A"}}>Date</Text>
<TouchableOpacity onPress={this._showDateTimePicker}  style={{backgroundColor:"white", width:(dimensions.fullWidth)/2,height:50,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth-10)/2-20,fontSize:15,textAlign:"right",fontWeight:"bold",color: this.state.dateset === "Select Date" ?  '#9A9A9A' :  'black'}}>{this.state.dateset}</Text> 
       <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
        />
      <View style={{paddingLeft:8}}><Image source={require("./Images/date.png")}  style={{height:15,width:15,tintColor:"grey"}}/></View>
</TouchableOpacity>

        </View>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
 
<TouchableOpacity onPress={() => this.props.navigation.navigate('accm')} style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>

<Text style={{  width:(dimensions.fullWidth-50),marginLeft:0,paddingLeft:5,fontSize:16,fontWeight:"bold",color: this.state.accmo === "Select Accommodation" ?  '#9A9A9A' :  'black'}}>{this.state.accmo}</Text>
<View style={{paddingLeft:8}}><Image source={require("./Images/rightarrow.png")}  style={{height:15,width:15,tintColor:"grey"}}/></View>

</TouchableOpacity>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<TextInput onChangeText={(text)=>this.setState({inputdesc:text})}  value={this.state.inputdesc} returnKeyType={'done'} keyboardType={"default"}  style={{width:dimensions.fullWidth-20,paddingLeft:5,fontSize:16,color:"black",marginLeft:0,fontWeight:"bold"}}placeholder={"Enter Description"}></TextInput>
</View>
</View>
 

<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{  width:(dimensions.fullWidth-20)/2,marginLeft:0,paddingLeft:5,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>Amount(£)</Text>
<TextInput onChangeText={(text)=>this.setState({amount:text})} value={this.state.amount}  returnKeyType={'done'} keyboardType={"decimal-pad"}  style={{ textAlign:"right",paddingRight:5,width:(dimensions.fullWidth-20)/2,fontSize:16,color:"#9A9A9A",marginLeft:1,fontWeight:"bold", }}placeholder={"0.00"}></TextInput></View>
</View>
 
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{  width:(dimensions.fullWidth-10)/2+95,marginLeft:0,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>TOTAL(£)</Text>
<Text returnKeyType={'done'} keyboardType={"decimal-pad"}  style={{ textAlign:"right",width:(dimensions.fullWidth-10)/3-55,fontSize:16,color:"#9A9A9A",marginLeft:1,fontWeight:"bold",}}>{this.getvalue(this.state.amount)}</Text></View>
</View>
   
</ScrollView> 
</View>:<View style={{width:dimensions.fullWidth,height:(dimensions.fullHeight-80),backgroundColor:"#EFEFF4"}}>












<ScrollView>
  {/* #F3F3F7 */}
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{width:(dimensions.fullWidth-10)/2-20,fontSize:15,paddingLeft:5,fontWeight:"bold",color:"#9A9A9A"}}>Date</Text>
<TouchableOpacity onPress={this._showDateTimePicker}  style={{backgroundColor:"white", width:(dimensions.fullWidth)/2,height:50,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth-10)/2-20,fontSize:15,textAlign:"right",fontWeight:"bold",color: this.state.dateset === "Select Date" ?  '#9A9A9A' :  'black'}}>{this.state.dateset}</Text> 
       <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
        />
      <View style={{paddingLeft:8}}><Image source={require("./Images/date.png")}  style={{height:15,width:15,tintColor:"grey"}}/></View>
</TouchableOpacity>

        </View>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
 
<TouchableOpacity onPress={() => this.props.navigation.navigate('accm')} style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>

<Text style={{  width:(dimensions.fullWidth-50),marginLeft:0,paddingLeft:5,fontSize:16,fontWeight:"bold",color: this.state.accmo === "Select Accamodation" ?  '#9A9A9A' :  'black'}}>{this.state.accmo}</Text>
<View style={{paddingLeft:8}}><Image source={require("./Images/rightarrow.png")}  style={{height:15,width:15,tintColor:"grey"}}/></View>

</TouchableOpacity>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<TextInput onChangeText={(text)=>this.setState({inputdesc:text})} returnKeyType={'done'} keyboardType={"default"}  style={{width:dimensions.fullWidth-20,paddingLeft:5,fontSize:16,color:"black",marginLeft:0,fontWeight:"bold"}}placeholder={"Enter Description"}></TextInput>
</View>
</View>
 

<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{  width:(dimensions.fullWidth-20)/2,marginLeft:0,paddingLeft:5,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>Amount(£)</Text>
<TextInput onChangeText={(text)=>this.myValue1(text)} value={this.state.amount}  returnKeyType={'done'} keyboardType={"decimal-pad"}  style={{ textAlign:"right",paddingRight:5,width:(dimensions.fullWidth-20)/2,fontSize:16,color:"#9A9A9A",marginLeft:1,fontWeight:"bold", }}placeholder={"0.00"}></TextInput></View>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{  width:(dimensions.fullWidth-20)/2,marginLeft:0,paddingLeft:5,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>VAT(£)</Text>
<TextInput onChangeText={(text)=> this.myValue(text) } value={this.state.hoursrate}  returnKeyType={'done'} keyboardType={"decimal-pad"}  style={{ textAlign:"right",paddingRight:5,width:(dimensions.fullWidth-20)/2,fontSize:16,color:"#9A9A9A",marginLeft:1,fontWeight:"bold", }}placeholder={"0.00"}></TextInput></View>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{  width:(dimensions.fullWidth-10)/2+95,marginLeft:0,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>TOTAL(£)</Text>
<Text returnKeyType={'done'} keyboardType={"decimal-pad"}  style={{ textAlign:"right",width:(dimensions.fullWidth-10)/3-55,fontSize:16,color:"#9A9A9A",marginLeft:1,fontWeight:"bold",}}>{this.getvalue(this.state.totals)}</Text></View>
</View>

<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
 <TouchableOpacity onPress={this.handleChoosePhoto} style={{backgroundColor:"white",width:(dimensions.fullWidth-20)/2-30,borderWidth:1,borderRadius:5,borderColor:"black",marginLeft:10,height:40,justifyContent:"center",alignItems:"center"}}>
 <Text style={{fontSize:16,textAlign:"center",fontWeight:"bold",color:"#9A9A9A"}}>ADD RECIEPT</Text>
 </TouchableOpacity>
 <View style={{backgroundColor:"white",width:(dimensions.fullWidth-20)/2-20,marginLeft:5,height:50,justifyContent:"center",alignItems:"center"}}>
 {/* <TouchableOpacity onPress={() => {this.setModalVisible(false)}}><Text onChangeText={(text) => this.state.userphoto == null ? this.state.textchange : this.state.reciept} style={{fontSize:16,fontWeight:"bold",textAlign:"center",color:"#9A9A9A"}}>{this.state.textchange}</Text></TouchableOpacity> */}
 {this.state.userphoto == null ? <Text onPress={() => {this.setModalVisible(false)}} style={{fontSize:16,fontWeight:"bold",textAlign:"center",color:"#9A9A9A"}}>No reciept...</Text>:<Text onPress={() => {this.setModalVisible(true)}} style={{fontSize:16,fontWeight:"bold",textAlign:"center",color:"#9A9A9A"}}>View reciept...</Text>}
  </View>                                                                                                                                                           
  {this.state.userphoto == null ? <View style={{backgroundColor:"white",height:50,width:40,justifyContent:"center",alignItems:"center"}}></View>:<TouchableHighlight  onPress={() => {this.delpik(this.state.userphoto)}}  style={{backgroundColor:"white",height:50,width:40,justifyContent:"center",alignItems:"center"}}><Image source={require("./Images/cross.png")}  style={{height:25,width:25,}}/></TouchableHighlight>}
 <Modal
          animationType="none"
          transparent={false}
          visible={this.state.modalVisible}
         
          // backgroundColor={"green"}
          // width={350}
          // height={350}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
           <View style={{backgroundColor:"white",flexDirection:"row",width:(dimensions.fullWidth),marginLeft:0,height:(dimensions.fullHeight-100),marginTop:50, justifyContent:"center",alignItems:"center"}}>
           <Image resizeMode={"stretch"} source={this.state.userphoto}  style={{width:'95%',height:'95%',margin:0,justifyContent:"center",alignSelf:"center"}}/>
          <View style={{height:(dimensions.fullHeight-100),backgroundColor:"white",marginTop:30}}>
          <TouchableOpacity activeOpacity={0.25}
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
               <Image source={require('./Images/cross.png')} style={{width:25,height:25,marginLeft:-25,marginTop:5}}/>
               </TouchableOpacity> 
              </View>
              </View>
        </Modal>
</View>
</View>
  
        
</ScrollView>

</View>}

        
        </View>
        <NavigationEvents onDidFocus={() => this.event()} />
        </SafeAreaView>
    );
  }
}
