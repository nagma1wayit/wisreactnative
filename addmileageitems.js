import React, { Component } from 'react'
import { Text, View,SafeAreaView,ScrollView,TextInput,TouchableOpacity,Image,StyleSheet,Dimensions,FlatList} from 'react-native'
import { SwipeRow, Icon, Button } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }
  export default class newmileage extends Component {
    constructor(props) {
      super(props);
      this.state = {
        dateset:"Select Date",
        isDateTimePickerVisible: false,
        refreshing: false,
       data: [ { "name": "TES-20190212-37251", "date": "12-02-2019" },
       {"name": "Ebony Maw","date": "13-02-2019"    },
       {"name": "Black Dwarf","date": "14-02-2019"  },
       {"name": "Mad Titan","date": "15-02-2019"    },
       {"name": "Mad Titan","date": "16-02-2019"    },
       {"name": "Mad Titan","date": "17-02-2019"    },
       {"name": "Mad Titan","date": "18-02-2019"    },
       {"name": "Mad Titan","date": "19-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Supergiant","date": "21-02-2019"   },
       { "name": "TES-20190212-37251","date": "12-02-2019"},
       { "name": "TES-20190212-37251","date": "12-02-2019"},
       { "name": "TES-20190212-37251","date": "12-02-2019"},],
      };
      this.component = [];
      this.selectedRow;
    }
    gobackAction=()=>{

        this.props.navigation.goBack()
            
             
        }
        empty = () => {
          return (
            <View style={{height:(dimensions.fullHeight)/5-60,width:dimensions.fullWidth,backgroundColor:"white"}}/>
          );
        };
        _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
 
        _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

        _handleDatePicked = (date) => {
 // console.log('A date has been picked: ', date);
        var dateObj = new Date(date);
        var momentObj = moment(dateObj);
        var momentString = momentObj.format("DD-MM-YYYY");
        this.setState({dateset:momentString});
   
        this._hideDateTimePicker();
};

  render() {
    var dlist =[{value: 'Bike'},{value: 'Bicycle'},{value: 'Car'},]
    return (
      <SafeAreaView>
      <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"white",marginTop:0}}>
    <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
     <TouchableOpacity onPress = {this.gobackAction}  style={{width:40, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
     <Image source={require("./Images/leftarrow.png")}  style={{height:20,width:25,tintColor:"white"}}/>
     </TouchableOpacity>
     <Text style={{ width:dimensions.fullWidth-100,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>ADD ITEM</Text>
     <TouchableOpacity style={{ width:40,height:30,top:18,marginLeft:5,justifyContent:"center",alignItems:"center"}}><Text style={{color:"white",fontSize:16,fontWeight:"normal",}}>Save</Text></TouchableOpacity>
     </View>
     <View style={{width:dimensions.fullWidth,height:(dimensions.fullHeight-80),backgroundColor:"#EFEFF4"}}>
<ScrollView>
  {/* #F3F3F7 */}
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{width:(dimensions.fullWidth-10)/2-20,fontSize:15,paddingLeft:5,fontWeight:"bold",color:"#9A9A9A"}}>Mileage Date</Text>
<TouchableOpacity onPress={this._showDateTimePicker}  style={{backgroundColor:"white", width:(dimensions.fullWidth)/2,height:50,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth-10)/2-20,fontSize:15,textAlign:"right",fontWeight:"bold",color: this.state.dateset === "Select Date" ?  '#9A9A9A' :  'black'}}>{this.state.dateset}</Text> 
       <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
          minimumDate={new Date()}
        />
      <View style={{paddingLeft:8}}><Image source={require("./Images/date.png")}  style={{height:15,width:15,tintColor:"grey"}}/></View>
</TouchableOpacity>

        </View>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{  width:(dimensions.fullWidth-20)/2,marginLeft:0,paddingLeft:5,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>From</Text>
<TextInput returnKeyType={'done'} keyboardType={"decimal-pad"}  style={{ textAlign:"right",paddingRight:2,width:(dimensions.fullWidth-20)/2,fontSize:16,color:"#9A9A9A",marginLeft:1,fontWeight:"bold"}}></TextInput></View>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{  width:(dimensions.fullWidth-20)/2,marginLeft:0,paddingLeft:5,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>To</Text>
<TextInput returnKeyType={'done'} keyboardType={"decimal-pad"}  style={{ textAlign:"right",paddingRight:2,width:(dimensions.fullWidth-20)/2,fontSize:16,color:"#9A9A9A",marginLeft:1,fontWeight:"bold"}}></TextInput></View>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{  width:(dimensions.fullWidth-20)/2,paddingLeft:5,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>Method</Text>
<View style={{width:(dimensions.fullWidth-20)/2,height:50,backgroundColor:"white",justifyContent:"center",alignItems:"flex-end"}}>
<Dropdown
         data={dlist}//your data collection
         labelFontSize={0}
         pickerStyle={{width:100,height:150,marginLeft:(dimensions.fullWidth-10)/2+70,backgroundColor:"white"}}
         baseColor={'#000'}
         fontSize={16}
         fontWeight={'normal'}
         dropdownOffset={{top:0}}
         containerStyle={{backgroundColor: 'white',  marginTop:0,marginLeft:0, padding: 0,width:(dimensions.fullWidth-10)/4,height:30,}} //replace windowHeight with your own
         value={'Car'}
         rippleOpacity={0}
         style={{ backgroundColor: 'transparent', padding: 0, paddingLeft:2 }} //replace size with your own
         />
         </View>
</View>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<TextInput returnKeyType={'done'} keyboardType={"decimal-pad"}  style={{width:dimensions.fullWidth-20,paddingLeft:5,fontSize:16,color:"#9A9A9A",marginLeft:0,fontWeight:"bold"}}placeholder={"Purpose...."}></TextInput>
</View>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{  width:(dimensions.fullWidth-20)/2,marginLeft:0,paddingLeft:5,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>Miles</Text>
<TextInput returnKeyType={'done'} keyboardType={"decimal-pad"}  style={{ textAlign:"right",paddingRight:2,width:(dimensions.fullWidth-20)/2,fontSize:16,color:"#9A9A9A",marginLeft:1,fontWeight:"bold"}}placeholder={"0.00"}></TextInput></View>
</View>
<View style={{backgroundColor:"#EFEFF4",height:70,width:dimensions.fullWidth,justifyContent:"center",alignItems:"center"}}>
<View style={{backgroundColor:"white",height:50,width:(dimensions.fullWidth-20),marginLeft:0,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
<Text style={{  width:(dimensions.fullWidth-10)/2+95,marginLeft:0,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>Mileage Expense(£)</Text>
<Text returnKeyType={'done'} keyboardType={"decimal-pad"}  style={{ textAlign:"right",paddingRight:2,width:(dimensions.fullWidth-10)/3-55,fontSize:16,color:"#9A9A9A",marginLeft:1,fontWeight:"bold"}}>0.00</Text></View>
</View>


        
        
        
        
        
        
        
        
        
        
</ScrollView>

</View>

        
        </View>
        </SafeAreaView>
    );
  }
}
