import React, { Component } from 'react';
import { View, Text,SafeAreaView,ScrollView ,Dimensions,TouchableOpacity,Image} from 'react-native';
const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }
export default class selectcustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  toggleDrawer1 = () => {
 
    console.log(this.props.navigation);
 
    this.props.navigation.toggleDrawer();
 
  }

  render() {
    return (
      
      <SafeAreaView>
      <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"white",marginTop:0}}>
    <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
     <TouchableOpacity onPress={this.toggleDrawer1}  style={{width:40,  height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
     <Image source={require("./Images/menu.png")}  style={{height:20,width:20}}/>
     </TouchableOpacity>
     <Text style={{ width:dimensions.fullWidth-65,height:30,top:18,marginLeft:0,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>CONTACT US</Text>
     </View> 
     <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:(dimensions.fullHeight-20)/2}}>
     <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:40,flexDirection:"row",alignItems:"center"}}><View style={{backgroundColor:"white",marginLeft:5,height:40,width:30,justifyContent:"center",alignItems:"center"}}><Image source={require("./Images/mail.png")}  style={{height:20,width:20,tintColor:"#2685FF"}}/></View><Text style={{fontSize:16}}>:</Text><TouchableOpacity><Text style={{fontSize:16,color:"#2685FF",textDecorationLine:"underline",marginLeft:5}}>info@wis-accountancy.co.uk</Text></TouchableOpacity></View>
     <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:40,flexDirection:"row",alignItems:"center"}}><View style={{backgroundColor:"white",marginLeft:5,height:40,width:30,justifyContent:"center",alignItems:"center"}}><Image source={require("./Images/phone.png")}  style={{height:20,width:20,tintColor:"#2685FF"}}/></View><Text style={{fontSize:16}}>:</Text><TouchableOpacity><Text style={{fontSize:16,color:"#2685FF",textDecorationLine:"underline",marginLeft:5}}>02030111898</Text></TouchableOpacity></View>
     <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:40,flexDirection:"row",alignItems:"center"}}><View style={{backgroundColor:"white",marginLeft:5,height:40,width:30,justifyContent:"center",alignItems:"center"}}><Image source={require("./Images/location16.png")}  style={{height:20,width:20,tintColor:"#2685FF"}}/></View><Text style={{fontSize:16}}>:</Text><TouchableOpacity><Text style={{textAlign:"auto",fontSize:16,color:"#2685FF",textDecorationLine:"underline",marginLeft:5,}}>4,Imperial Place, Maxwell Road,Borehamwood,WD61JN</Text></TouchableOpacity></View>
     <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:40,flexDirection:"row",alignItems:"center"}}><View style={{backgroundColor:"white",marginLeft:5,height:40,width:30,justifyContent:"center",alignItems:"center"}}><Image source={require("./Images/website.png")}  style={{height:20,width:20,tintColor:"#2685FF"}}/></View><Text style={{fontSize:16}}>:</Text><TouchableOpacity><Text style={{fontSize:16,color:"#2685FF",textDecorationLine:"underline",marginLeft:5}}>www.wisaccountancy.co.uk</Text></TouchableOpacity></View>
     <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:20,flexDirection:"row",alignItems:"center"}}><TouchableOpacity style={{marginLeft:35}}><Text style={{fontSize:16,color:"#2685FF",textDecorationLine:"underline",marginLeft:10}}>www.wismortgages.co.uk</Text></TouchableOpacity></View>
    <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:100,flexDirection:"row",justifyContent:"center",alignItems:"center"}}><View style={{backgroundColor:"white",justifyContent:"center",alignItems:"center",height:80,width:80}}><TouchableOpacity><Image source={require("./Images/facebook.png")}  style={{height:30,width:30,}}/></TouchableOpacity></View><View style={{backgroundColor:"white",justifyContent:"center",alignItems:"center",height:80,width:80}}><TouchableOpacity><Image source={require("./Images/twitter.png")}  style={{height:30,width:30,}}/></TouchableOpacity></View><View style={{backgroundColor:"white",justifyContent:"center",alignItems:"center",height:80,width:80}}><TouchableOpacity><Image source={require("./Images/linkedin.png")}  style={{height:30,width:30}}/></TouchableOpacity></View></View>
     </View>
     
     
     
     
     </View> 
       </SafeAreaView>
    );
  }
}
