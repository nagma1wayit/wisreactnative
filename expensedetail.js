import React, { Component } from 'react'
import { Text, View,SafeAreaView,AsyncStorage,navigation,TouchableOpacity,Image,ScrollView,StyleSheet,Dimensions,FlatList} from 'react-native'
import { SwipeRow, Icon, Button } from 'native-base';
 
const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }
  export default class expensedetail extends Component {
    constructor(props) {
      super(props);
      this.state = {
      expenseid:"",
      expensedeta:"",
      data:[],
      data1:[],
      apk:'',
      monthchange:"",
       
      
      };
      
    }
    componentDidMount = () =>{
      console.disableYellowBox = true
      AsyncStorage.getItem("apk").then((value) => {
      
        this.setState({apk: value});
        console.log("hhj",this.state.apk)
        this.expense();
      }).done();
      const { navigation } = this.props;
      const recexpdeta = navigation.getParam('RemainData');
      console.log("recieve",recexpdeta)
      this.setState({expenseid:recexpdeta.ID,  expensedeta:recexpdeta});
     
    }
    gobackAction=()=>{
       
        this.props.navigation.goBack()
            
             
        }
        expense (){
          
 
          fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/expense/' + this.state.expenseid+'/'+ this.state.apk)
             .then((response) => response.json())
             .then((responseJson) => {
        
        
               console.log("expnese detail:",responseJson)
               
               this.setState({
                data:responseJson[0].Expense,
                data1:responseJson[0].Expense.ExpenseItems[0],
                
               })
               var monthword=responseJson[0].Expense.Month
                   
                  if(monthword == "1"){
                    this.setState({monthchange:"January"})
                    return(this.state.monthchange)
                       }else if(monthword == "2"){
                        this.setState({monthchange:"February"})
                        return(this.state.monthchange)
                       }else if(monthword == "3"){
                        this.setState({monthchange:"March"})
                        return(this.state.monthchange)
                       }else if(monthword == "4"){
                        this.setState({monthchange:"April"})
                        return(this.state.monthchange)
                       }else if(monthword == "5"){
                        this.setState({monthchange:"May"})
                        return(this.state.monthchange)
                       }else if(monthword == "6"){
                        this.setState({monthchange:"June"})
                        return(this.state.monthchange)
                       }else if(monthword == "7"){
                        this.setState({monthchange:"July"})
                        return(this.state.monthchange)
                       }else if(monthword == "8"){
                        this.setState({monthchange:"August"})
                        return(this.state.monthchange)
                       }else if(monthword == "9"){
                        this.setState({monthchange:"September"})
                        return(this.state.monthchange)
                       }else if(monthword == "10"){
                        this.setState({monthchange:"October"})
                        return(this.state.monthchange)
                       }else if(monthword == "11"){
                        this.setState({monthchange:"November"})
                        return(this.state.monthchange)
                       }else if(monthword == "12"){
                        this.setState({monthchange:"December"})
                        return(this.state.monthchange)
                       }
                   
               
             })
             .catch((error) =>{
               console.log(error);
             });
           }
         
           getvalue = (allval) => {
           
             if(allval == 0){
                
                return "0.00"
                
             }else{
           
               var value = 0;
               value = parseFloat(allval).toFixed(2);
           
             return value
           
             } 
              
           }
       
           colorchange = function(allvalue) {
            if(allvalue <0 ){
              
              return {width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"red",textAlign:"right"}
           
            }else{
               
              return {width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"black",textAlign:"right"}
            }
             
          }
          invoicedetailitem = (expeitem) =>{
  
            var expoitems =expeitem.ExpenseItems
            console.log("invo",expoitems)
            
            
             this.props.navigation.navigate("expitem",{expoitems})
             // console.log("data send",this.props.navigation.navigate("expitem",{expoitems})
             // )
              
           
          }

  render() {
    console.log("ids",this.state.expenseid)
    console.log("remainig",this.state.expensedeta)
 
    return (
        <SafeAreaView  style={{backgroundColor:"white",flex:1}}>
        <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"white",marginTop:0}}>
       <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
        <TouchableOpacity onPress = {this.gobackAction}  style={{ width:40, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
        <Image source={require("./Images/leftarrow.png")}  style={{height:20,width:25,tintColor:"white"}}/>
        </TouchableOpacity>
        <Text style={{width:dimensions.fullWidth-80,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>EXPENSE</Text>
        </View>
        
        <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:(dimensions.fullHeight-80)}}>
        <ScrollView>
      <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:600}}>
         <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:100,justifyContent:"center",alignItems:"center"}}>
         <View style={{justifyContent:"center",alignSelf:"center",width:60,height:60,backgroundColor:"white"}}>
          <Image source={require("./Images/bluetik.png")}  style={{width:50,height:50,justifyContent:"center",alignSelf:"center"}}/> 

          </View>
          </View>

           <View style={{width:dimensions.fullWidth,height:50,backgroundColor:"#B8B8B8",justifyContent:"center",alignItems:"center"}}>
       
       <Text style={{fontSize:18,fontWeight:"500",color:"white",justifyContent:"center",textAlign:"center",}}>CREATED</Text>
       </View>

      <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"White",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Expense number</Text>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"black",textAlign:"right"}}>{this.state.data.ExpenseNumber}</Text>
           </View>
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"White",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Employee Name</Text>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"black",textAlign:"right"}}>{this.state.data.EmployeeName}</Text>
           </View>           
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"White",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Month</Text>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"black",textAlign:"right"}}>{this.state.monthchange}</Text>
           </View>
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"White",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Year</Text>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"black",textAlign:"right"}}>{this.state.data.Year}</Text>
           </View> 
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"White",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Total Amount</Text>
              <Text style={this.colorchange(this.state.data.TotalAmount)}>£ {this.getvalue(this.state.data.TotalAmount)}</Text>
           </View>           
           <TouchableOpacity onPress={() => this.invoicedetailitem(this.state.data)} style={{width:dimensions.fullWidth,height:40,backgroundColor:"#EFEFF4",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2+155,fontSize:16,fontWeight:'bold',color:"#318AE1",justifyContent:"center",textAlign:"center"}}>View All Expense Items</Text>
          <Image source={require("./Images/rightarrow.png")}  style={{height:20,width:20,tintColor:"grey",justifyContent:"center",alignSelf:"center"}}/> 
            </TouchableOpacity>
                     
           
        



        </View>
       </ScrollView> 

    </View>
        </View>
        </SafeAreaView>
    );
  }
}
 