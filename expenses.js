import React, { Component } from 'react';
import { View, Text,SafeAreaView,AsyncStorage,Dimensions,RefreshControl,ActivityIndicator,TouchableOpacity,StyleSheet,Image,FlatList,Alert} from 'react-native';
const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}
 
import { SwipeRow, Icon, Button } from 'native-base';
 
export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ColorHolder : "green",
      refreshing: false, 
      selectedRow : '',
      Id:"", 
      apk:'',
      datas: [],
      isLoading: true,
      fetching_from_server: false,  
      VATTYPES:"",
     
  };
  this.component = [];
  this.offset = 0;
  //this.state.selectedRow;
  }
  componentDidMount(){


   
    AsyncStorage.getItem("apk").then((value) => {
      
      this.setState({apk: value});
      console.log(this.state.apk)
      this.expenses()
    
  }).done();
   }
   expenses (){
    
    fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/expenses/'+ this.offset +'/EXPENSE/' + this.state.apk)
       .then((response) => response.json())
       .then((responseJson) => {
        console.log("dataoin",responseJson)
        this.offset = this.offset + 1;
  
         
         const clintvat = responseJson[0].clientVatType

         AsyncStorage.setItem("clivat",clintvat);
         this.setState({VATTYPES:clintvat})
           
          this.setState({
            datas: [...this.state.datas, ...responseJson[0].Expenses],
            
            isLoading: false,
             
          });
         
         
       })
       .catch((error) =>{
         console.log(error);
       });
     }
     loadData = () => {
  console.log("api key ",this.state.apk)
      this.setState({ fetching_from_server: true }, () => {
        fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/expenses/'+ this.offset +'/EXPENSE/' + this.state.apk)
        .then((response) => response.json())
            .then(responseJson => {
            console.log("printdata",responseJson)
              this.offset = this.offset + 1;
            
              this.setState({
                datas: [...this.state.datas, ...responseJson[0].Expenses],
                 fetching_from_server: false,
               });
            })
            .catch(error => {
              console.error(error);
            });
      });
    };
  
   
   toggleDrawer1 = () => {
 
    console.log(this.props.navigation);
 
    this.props.navigation.toggleDrawer();
 
  }
  
  
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#CED0CE",
          marginTop:0
        }}
        />
    );
  };
   
  ConvertTextToUpperCase=(val)=>{
 console.log("employename",val.EmployeeName)
if(val.EmployeeName == null){

}else{
      var str = val.EmployeeName;
      var char =  str.charAt(0).toUpperCase() 
      return char;
    
}
    
 
   }
  _getRandomColor(){
     
    var ColorCode = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 268)) + ',' + (Math.floor(Math.random() * 278)) + ')';
 
 return ColorCode;
  }
  pressdel = (val) =>{
   
   
    //this.state.selectedRow._root.closeRow();  
    {Alert.alert('Delete Expense',
    'Are you sure you want to delete expense ?',
    [
      {
        text: 'No',
       
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Yes',
       
        onPress: () =>{ 
          
           var array = [...this.state.datas]; // make a separate copy of the array
           var indexes = array.indexOf(val)
           array.splice(indexes, 1);
           this.setState({datas: array});
            
         
      }
        
      },
    ],)}
  }
   
  presscopy = (vals) =>{
    // this.selectedRow._root.closeRow();
    // {Alert.alert('Copy Expense',
    //   'Are you sure you want to copy expense',
    //   [
    //     {
    //       text: 'Cancel',
         
    //       onPress: () => console.log('Cancel Pressed'),
    //       style: 'cancel',
    //     },
    //     {
    //       text: 'Yes',
           
    //       onPress: () =>{ 
    //       var array = [...this.state.data];
    //       array.push(vals)
    //       this.setState({data: array})
           
    //     }
        
    //     },
    //   ],)}
    }
   
  _onRefresh = () => {
    this.setState({refreshing: true});
    this._getRandomColor(),
    this.setState({refreshing: false});
   
  }
  empty = () => {
    return (
      <View style={{height:(dimensions.fullHeight)/5-40,width:dimensions.fullWidth,backgroundColor:"white",alignItems:"center"}}>
      <TouchableOpacity  onPress = { this.loadData } style = { styles.loadMoreBtn }>
              <Text style = { styles.btnText }>Load More</Text>
              {
                  ( this.state.fetching_from_server )
                  ?
                  <ActivityIndicator  color="white" size="small" style={{ marginLeft: 8  }} />
                  : null
              }
          </TouchableOpacity>     
     
        </View>
    );
  };
  fun = (stat) => {
    if(stat.Status == "2"){
    var crt = "CREATED"
    var status1 = crt;
     //console.log(status1)
     return status1;
    }
    else if (stat.Status == "1"){
      var crts = "DRAFT";
      var status2 = crts;
      return status2;
    //  console.log("draft")
     
   } 
    else{
      console.log("ok")
    }
     
  }
  months = (mt) =>{
    if(mt.Month == "1"){
      var mon = "January"
      var month1 = mon;
       //console.log(status1)
       return month1;
      }
      else if(mt.Month == "2"){
        var feb = "February"
        var month2 = feb;
         //console.log(status1)
         return month2;
        }
        else if(mt.Month == "3"){
          var Mar = "March"
          var month3 = Mar;
           //console.log(status1)
           return month3;
          }
          else if(mt.Month == "4"){
            var Apr = "April"
            var month4 = Apr;
             //console.log(status1)
             return month4;
            }
            else if(mt.Month == "5"){
              var May = "May"
              var month5 = May;
               //console.log(status1)
               return month5;
              }
              else if(mt.Month == "6"){
                var Jun = "June"
                var month6 = Jun;
                 //console.log(status1)
                 return month6;
                }
                else if(mt.Month == "7"){
                  var Jul = "July"
                  var month7 = Jul;
                   //console.log(status1)
                   return month7;
                  }
                  else if(mt.Month == "8"){
                    var Aug = "August"
                    var month8 = Aug;
                     //console.log(status1)
                     return month8;
                    }
                    else if(mt.Month == "9"){
                      var Sep = "September"
                      var month9 = Sep;
                       //console.log(status1)
                       return month9;
                      }
                      else if(mt.Month == "10"){
                        var Oct = "October"
                        var month10 = Oct;
                         //console.log(status1)
                         return month10;
                        }
                        else if(mt.Month == "11"){
                          var Nov = "November"
                          var month11 = Nov;
                           //console.log(status1)
                           return month11;
                          }
                          else if(mt.Month == "12"){
                            var Dec = "December"
                            var month12 = Dec;
                             //console.log(status1)
                             return month12;
                            }
      else{
        console.log("ok")
      }
  }
   
  getvalue = (amount) => { 

    // var Tamount = amount.TotalVATAmount;
    // console.log(Tamount)
    // var Damount = amount.diffTotalAmount
    // console.log(Damount)
    //  var Total = parseInt(Tamount+Damount);
    //  console.log(Total)
     var Total =  amount.TotalAmount
    if(Total == 0){
       
       return "0.00"
       
    }else{ 
  
      var value = 0;
      value = parseFloat(Total).toFixed(2);
      console.log("iteprice",value)
     return value
  
    } 
     
  }  
  expensedetail = (stat3) =>{
    console.log("getstat3",stat3)
     
  
    var RemainData=stat3
    if(stat3.Status == "1"){
      
      this.props.navigation.navigate("newexp",{RemainData})
    }else{
     this.props.navigation.navigate("expenseDetail",{RemainData})
       
      
    }
  }
  render() {
    if (this.state.isLoading) {
           
      return (
        <View style={{ flex: 1, paddingTop: 0,justifyContent:"center",alignSelf:"center" }}>
          <View style={{backgroundColor:"#F3F3F7",borderRadius:10,height:70,width:70,justifyContent:"center",alignItems:"center"}}><ActivityIndicator size="large" color="grey"/></View>
        </View>
      );
    } 
    return (
      <SafeAreaView style={{backgroundColor:"white",flex:1}}>
      <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"pink",marginTop:0}}>
      <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
       <TouchableOpacity onPress={this.toggleDrawer1}  style={{width:50, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
       <Image source={require("./Images/menu.png")}  style={{height:20,width:20}}/>
       </TouchableOpacity>
       <Text style={{ width:dimensions.fullWidth-125,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>EXPENSES</Text>
       </View> 
      
       {/* <TouchableOpacity style={{backgroundColor:"white",width:dimensions.fullWidth,height:(dimensions.fullHeight-75)}}> */}
           <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:(dimensions.fullHeight-75)}}>
            <FlatList
            refreshControl={
            <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          data={this.state.datas}
          //extraData={this.state.data}
         ItemSeparatorComponent={this.renderSeparator}
         renderItem={({item,index}) => 
         <SwipeRow ref={(c) => { this.component[item,index] = c }}
          rightOpenValue={ this.state.VATTYPES == item.VatType ? -150:-300} 
          backgroundColor={"orange"}
          onRowOpen ={() => {
            this.component[item,index]
           if (this.state.selectedRow && this.state.selectedRow !== this.component[item,index]) { this.state.selectedRow._root.closeRow(); }
           //this.state.selectedRow = this.component[item.ID]

              var countData = this.state.datas.length+1;
              //this.state.countData;

              if (index == countData-1){
                
                this.state.selectedRow = this.component[item,index-1]
              //  this.state.selectedRow = this.component[index-1]
                
               }
               else {
                
                this.state.selectedRow = this.component[item,index ]
                 
               // this.state.selectedRow = this.component[index]
                
              }
              
         }}
         
        //   <SwipeRow ref={(c) => { this.component[item,index] = c }}
        //   rightOpenValue={-150}
        //   backgroundColor={"white"}
           
        //   onRowOpen={() => {
           
        //    if (this.state.selectedRow && this.state.selectedRow != this.component[item,index])
        //     { 
        //         this.state.selectedRow._root.closeRow();
              
        //       }

              // var countData = this.state.datas.length;
              // this.state.countData;

              // if (index == countData-1){
                
              //   this.state.selectedRow = this.component[item,index-1]
                
              //  }
              //  else {
                
              //   this.state.selectedRow = this.component[item,index ]
                 
              
              // }
              
               
 
        //  }}
         
        right={
          this.state.VATTYPES == item.VatType ? <View style={{flexDirection:"row",flex:1}}>
            
       <TouchableOpacity onPress = {this.pressdel} style={{backgroundColor:"red", flex: 1,
       alignItems: 'center',
       justifyContent: 'center',
       flexDirection: 'row'}}>
       <View><Image style={{height:30,width:30,tintColor:"white"}}  source = {require("./Images/delete.png")}/></View>
       </TouchableOpacity>
       
       <TouchableOpacity onPress = {this.presscopy} style={{backgroundColor:"lightseagreen", flex: 1,
         alignItems: 'center',
         justifyContent: 'center',
         flexDirection: 'row'}}>
         <View><Image style={{height:30,width:30,tintColor:"white"}}  source = {require("./Images/copy.png")}/></View>
       </TouchableOpacity>
       </View>:<View style={{flexDirection:"row",flex:1,backgroundColor:"#aaaaaa",justifyContent:"center",alignItems:"center"}}>
           
       <Text style={{fontFamily: 'Verdana',fontSize:16,fontWeight:"normal",color:"white",justifyContent:"center",textAlign:"center"}}>Please Change your VAT Reg. Type</Text>
       </View>}
        
        body={
          <TouchableOpacity onPress={() => this.expensedetail(item)} style={styles.flatview}>
          <View style={[styles.randomcontainer, { backgroundColor: this._getRandomColor()}]}><Text style={{fontSize:20,fontWeight:"300",color:"white"}}>{this.ConvertTextToUpperCase(item)}</Text></View>
            <View style={{backgroundColor:"white" ,marginLeft:5,width:(dimensions.fullWidth-20)/2+25, }}>
            <Text style={{fontFamily: 'Verdana',fontSize:16,fontWeight:"bold",color:"black"}}>{item.ExpenseNumber}</Text>
            <View style={{backgroundColor:"white",flexDirection:"row",paddingTop:10}}>
            <Text style={{color:"grey",fontSize:14}}>Expense Date:</Text>
            <Text numberOfLines={2} style={{color:"black",fontSize:14,fontWeight:"bold",paddingLeft:5}}>{this.months(item)} {item.Year}</Text>

            </View>
            </View>
            <View style={{backgroundColor:"white",width:(dimensions.fullWidth-20)/4+5,justifyContent:"center",alignItems:"center"}}>
            <View style={{backgroundColor: item.Status  === "1" ?  '#2D6BA0' :  '#2685E1' ,width:(dimensions.fullWidth-20)/4,justifyContent:"center",alignItems:"center",borderRadius:5,height:30}}><Text style={{fontSize:15,fontWeight:"bold",color:"white"}}>{this.fun(item)}</Text></View>
            <Text style={{color:"#128C0D",marginTop:5}}>£ {this.getvalue(item)}</Text></View>
            </TouchableOpacity>
       }>
        
          
          </SwipeRow>}
  
            

        
        keyExtractor={item => item.email}
        ListFooterComponent={this.empty()}
       />
 
 
       <TouchableOpacity onPress={() => this.props.navigation.navigate("newexp")} activeOpacity={0.5}  style={{backgroundColor:"#2685E1",width:60,height:60,borderRadius:30,bottom:(dimensions.fullHeight-20)/4-130,left:(dimensions.fullWidth)/2+90,justifyContent:"center",alignItems:"center",position:"absolute"}}>
       <Text style={{color:"white",fontSize:30,fontWeight:"normal"}}>+</Text></TouchableOpacity>
       {/* </TouchableOpacity> */}
       </View> 
      </View>
       </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  
  headerText: {
    fontSize: 15,
    textAlign: "center",
    margin: 10,
     backgroundColor:"orange",
    fontWeight: "bold"
  },
  flatview: {
     height:50,
    justifyContent: 'center',
    paddingTop: 0,
    borderRadius: 1,
    alignItems:"center",
    flexDirection:"row",
    backgroundColor:"white"
  },
  randomcontainer:{
    width:55,height:55,borderRadius:27.5,marginLeft:5,justifyContent:"center",alignItems:"center"
  },
  item: {
    padding: 10,
    fontSize: 20,
    height: 45,
    backgroundColor:"red"
  }, 
loadMoreBtn: {
  padding: 10,
  backgroundColor: '#2685E1',
  width:120,
  marginTop:10, 
  borderRadius: 4,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
},
btnText: {
  color: 'white',
  fontSize: 15,
  textAlign: 'center',
},
});