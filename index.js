/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
// import login from './login'
// import homes from './homes';

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
