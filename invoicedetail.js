import React, { Component } from 'react'
import { Text, View,SafeAreaView,AsyncStorage,navigation,TouchableOpacity,Image,ScrollView,StyleSheet,Dimensions,FlatList} from 'react-native'
import { SwipeRow, Icon, Button } from 'native-base';
 
const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }
  export default class newmileage extends Component {
    constructor(props) {
      super(props);
      this.state = {
      datarecieve:"",
      allremain:"",
      data:[],
      data1:[],
      apk:'',
      
      };
      
    }
    componentDidMount = () =>{
      console.disableYellowBox = true
      AsyncStorage.getItem("apk").then((value) => {
      
        this.setState({apk: value});
        console.log("hhj",this.state.apk)
        this.invoice();
      }).done();
      const { navigation } = this.props;
      const rece = navigation.getParam('remaindata');
      console.log("recieve",rece)
      this.setState({datarecieve:rece.InvoiceID, allremain:rece});
     
    }
    gobackAction=()=>{
       
        this.props.navigation.goBack()
            
             
        }
        invoice (){
          console.log("eve",this.state.datarecieve)
          console.log("hhj",this.state.apk)
 
          fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/invoice/itemId/' + this.state.datarecieve + '/API-KEY/' + this.state.apk)
             .then((response) => response.json())
             .then((responseJson) => {
        
        
               console.log("invoice detail:",responseJson)
               
                  console.log("name:",responseJson[0].Invoice.InvoiceItems[0].FlatRate)
                  console.log("items:",responseJson[0].Invoice.InvoiceItems)
             
                  var Quant= responseJson[0].Invoice.InvoiceItems[0].Quantity
                  var price= responseJson[0].Invoice.InvoiceItems[0].UnitPrice
                  
                   
               this.setState({
                data:responseJson[0].Invoice,
                data1:responseJson[0].Invoice.InvoiceItems[0],
                
               })
               
               
             })
             .catch((error) =>{
               console.log(error);
             });
           }
         
           getvalue = (allval) => {
           
             if(allval == 0){
                
                return "0.00"
                
             }else{
           
               var value = 0;
               value = parseFloat(allval).toFixed(2);
           
             return value
           
             } 
              
           }
       
           colorchange = function(allvalue) {
            if(allvalue <0 ){
              
              return {width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"red",textAlign:"right"}
           
            }else{
               
              return {width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"black",textAlign:"right"}
            }
             
          }
          invoicedetailitem = (dm) =>{
  
            var invoitems =dm.InvoiceItems
            console.log("invo",invoitems)
            
            
             this.props.navigation.navigate("invoiceitem",{invoitems})
              // console.log("data send",this.props.navigation.navigate("invoicedetails",stat2.InvoiceID)
              // )
              
           
          }

  render() {
    console.log("ids",this.state.datarecieve)
    console.log("remainig",this.state.allremain)
 
    return (
        <SafeAreaView  style={{backgroundColor:"white",flex:1}}>
        <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"white",marginTop:0}}>
       <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
        <TouchableOpacity onPress = {this.gobackAction}  style={{ width:40, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
        <Image source={require("./Images/leftarrow.png")}  style={{height:20,width:25,tintColor:"white"}}/>
        </TouchableOpacity>
        <Text style={{width:dimensions.fullWidth-80,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>INVOICE</Text>
        </View>
        
        <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:(dimensions.fullHeight-80)}}>
        <ScrollView>
      <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:600}}>
         <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:100,justifyContent:"center",alignItems:"center"}}>
         <View style={{justifyContent:"center",alignSelf:"center",width:60,height:60,backgroundColor:"white"}}>
          <Image source={require("./Images/bluetik.png")}  style={{width:50,height:50,justifyContent:"center",alignSelf:"center"}}/> 

          </View>
          </View>

           <View style={{width:dimensions.fullWidth,height:50,backgroundColor:"#B8B8B8",justifyContent:"center",alignItems:"center"}}>
       
       <Text style={{fontSize:18,fontWeight:"500",color:"white",justifyContent:"center",textAlign:"center",}}>CREATED</Text>
       </View>

      <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"#EFEFF4",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Invoice number</Text>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"black",textAlign:"right"}}>{this.state.data.InvoiceNumber}</Text>
           </View>
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"White",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Customer Name</Text>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"black",textAlign:"right"}}>{this.state.data.Name}</Text>
           </View>           
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"#EFEFF4",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Invoice Date</Text>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"black",textAlign:"right"}}>{this.state.data.InvoiceDate}</Text>
           </View>
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"White",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Due Date</Text>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,fontWeight:'bold',paddingRight:10,color:"black",textAlign:"right"}}>{this.state.data.DueDate}</Text>
           </View>           
           <TouchableOpacity onPress={() => this.invoicedetailitem(this.state.data)} style={{width:dimensions.fullWidth,height:40,backgroundColor:"#EFEFF4",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2+155,fontSize:16,fontWeight:'bold',color:"#318AE1",justifyContent:"center",textAlign:"center"}}>View All Invoice Items</Text>
          <Image source={require("./Images/rightarrow.png")}  style={{height:20,width:20,tintColor:"grey",justifyContent:"center",alignSelf:"center"}}/> 
            </TouchableOpacity>
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"White",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Amount</Text>
              <Text style={this.colorchange(this.state.allremain.SubTotal)}>£ {this.getvalue(this.state.allremain.SubTotal)}</Text>
           </View>            
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"#EFEFF4",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>VAT</Text>
              <Text style={this.colorchange(this.state.allremain.Tax)}>£ {this.getvalue(this.state.allremain.Tax)}</Text>
           </View>
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"white",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Total</Text>
              <Text style={this.colorchange(this.state.allremain.InvoiceTotal)}>£ {this.getvalue(this.state.allremain.InvoiceTotal)}</Text>
           </View>
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"#EFEFF4",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Flat Rate</Text>
              <Text  style={this.colorchange(this.state.data1.FlatRate)}>£ {this.state.data1.FlatRate}</Text>
           </View>
           <View style={{width:dimensions.fullWidth,height:40,backgroundColor:"White",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
              <Text style={{width:(dimensions.fullWidth)/2,fontSize:16,paddingLeft:10,fontWeight:'bold',color:"#9A9A9A"}}>Net Sales</Text>
              <Text style={this.colorchange(this.state.data1.NetSales)}>£ {this.state.data1.NetSales}</Text>
           </View>
        



        </View>
       </ScrollView> 

    </View>
        </View>
        </SafeAreaView>
    );
  }
}
 