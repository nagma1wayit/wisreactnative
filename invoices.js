import React, { Component } from 'react';
import { View, Text,SafeAreaView,Dimensions,AsyncStorage,RefreshControl,ActivityIndicator,TouchableOpacity,StyleSheet,Image,FlatList,Alert} from 'react-native';
const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}
 
import { SwipeRow, Icon, Button } from 'native-base';
 
export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ColorHolder : "green",
      refreshing: false,
      Id:"", 
      apk:'', 
      isLoading: true, 
      serverData: [], 
      fetching_from_server: false, 
      VATTYPE:""
       
  };
  this.component = [];
  this.selectedRow;
  this.offset = 0;
  }
  componentDidMount(){


   
    AsyncStorage.getItem("apk").then((value) => {
      
      this.setState({apk: value});
      console.log(this.state.apk)
    
      this.invoices()
    this.setState({isLoading:true})
  }).done();
   }
   

   invoices (){
    this.setState({isLoading:true})
    
    fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/invoices/page/'+ this.offset +'/API-KEY/' + this.state.apk)
       .then((response) => response.json())
       .then((responseJson) => {
       console.log("dataoin",responseJson)
        this.offset = this.offset + 1;
      
          
          const vatpercent=responseJson[0].configvatpercentage
          
          this.setState({VATTYPE:responseJson[0].clientVatType})
          AsyncStorage.setItem("vatper",vatpercent);
          
          this.setState({
            serverData: [...this.state.serverData, ...responseJson[0].Invoices],
            
            isLoading: false,
             
          });

       
       })
       .catch((error) =>{
         console.log(error);
       });
     }
  
       loadMoreData = () => {
  
    this.setState({ fetching_from_server: true }, () => {
      fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/invoices/page/'+ this.offset +'/API-KEY/' + this.state.apk+'/'+ this.offset)
         .then(response => response.json())
          .then(responseJson => {
          console.log("printdata",responseJson)
            this.offset = this.offset + 1;
          
            this.setState({
              serverData: [...this.state.serverData, ...responseJson[0].Invoices],
               fetching_from_server: false,
             });
          })
          .catch(error => {
            console.error(error);
          });
    });
  };
   
 
      
   toggleDrawer1 = () => {
 
    console.log(this.props.navigation);
 
    this.props.navigation.toggleDrawer();
 
  }
   
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#CED0CE",
          marginTop:0
        }}
        />
    );
  };

  ConvertTextToUpperCase=(val)=>{
 
if(val.Name == null){

}else{
  var str = val.Name;
      
  var char =  str.charAt(0)
  return char;
 
}
      
 
  }
  _getRandomColor(){
     
    var ColorCode = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 268)) + ',' + (Math.floor(Math.random() * 278)) + ')';
 
 return ColorCode;
  }
  pressdel = () =>{
    {Alert.alert('Delete Invoice',
    'Are you sure you want to delete invoice item ?',
    [
      {
        text: 'No',
       
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Yes',
         
        onPress: () => {this.deleteItemById()},
      },
    ],)}
  }
  presspdf = () =>{
    {Alert.alert('Pdf Download',
      'Are you sure you want to Download & Share Pdf ?',
      [
        {
          text: 'Cancel',
         
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Download & Share',
     
          onPress: () => console.log('ok Pressed'),
        },
      ],)}
  }
  presscopy = () =>{
    {Alert.alert('Copy Invoice',
      'Are you sure you want to copy invoice',
      [
        {
          text: 'Cancel',
         
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'ok',
          
          onPress: () => console.log('ok Pressed'),
        },
      ],)}
  }
  _delete(){
     const newState = this.state.serverData.slice();
     if (newState.indexOf(item) > -1) {
     newState.splice(newState.indexOf(item), 1);
     this.setState({serverData: newState})
    }
    
  }
  _onRefresh = () => {
    this.setState({refreshing: true});
    this._getRandomColor(), 
    this.setState({refreshing: false});
    
   
  }
  empty = () => {
    return (
      <View style={{height:(dimensions.fullHeight)/5-40,width:dimensions.fullWidth,backgroundColor:"white",alignItems:"center"}}>
      <TouchableOpacity  onPress = { this.loadMoreData } style = { styles.loadMoreBtn }>
              <Text style = { styles.btnText }>Load More</Text>
              {
                  ( this.state.fetching_from_server )
                  ?
                  <ActivityIndicator  color="white" size="small" style={{ marginLeft: 8  }} />
                  : null
              }
          </TouchableOpacity>     
     {/* {this.state.fetching_from_server ? (
        <ActivityIndicator  color="grey" size="large" style={{ marginLeft: 8,padding:10 }} />
        ) : null} */}
        </View>
    );
  };
fun = (stat) => {
  if(stat.Status == "2"){
  var crt = "CREATED"
  var status1 = crt;
   //console.log(status1)
   return status1;
   
  }
  else if (stat.Status == "1"){
    var crts = "DRAFT";
    var status2 = crts; 
    return status2;
   
   
 } 
  else{
    console.log("ok")
  }
   
}
invoicedetail = (stat2) =>{
  console.log("taxvax",stat2)
   

  var remaindata=stat2 
  if(stat2.Status == "1"){
    
    this.props.navigation.navigate("newinvoices",{remaindata})
  }else{
    this.props.navigation.navigate("invoicedetails",{remaindata})
    // console.log("data send",this.props.navigation.navigate("invoicedetails",stat2.InvoiceID)
    // )
    
  }
}
getvalue = (allvals) => { 

  if(allvals == 0){
     
     return "0.00"
     
  }else{ 

    var value = 0;
    value = parseFloat(allvals).toFixed(2);

  return value

  } 
   
} 
 


  render() {
    if (this.state.isLoading) {
           
      return (
        <View style={{ flex: 1, paddingTop: 0,justifyContent:"center",alignSelf:"center" }}>
          <View style={{backgroundColor:"#F3F3F7",borderRadius:10,height:70,width:70,justifyContent:"center",alignItems:"center"}}><ActivityIndicator size="large" color="grey"/></View>
        </View>
      );
    } 
    console.log("fltdata",this.state.serverData)
    return (
      <SafeAreaView style={{backgroundColor:"white",flex:1}}>
      <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"white",marginTop:0}}>
      <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
       <TouchableOpacity onPress={this.toggleDrawer1}  style={{width:50, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
       <Image source={require("./Images/menu.png")}  style={{height:20,width:20}}/>
       </TouchableOpacity>
       <Text style={{ width:dimensions.fullWidth-125,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>INVOICES</Text>
       </View> 
      
       {/* <TouchableOpacity style={{backgroundColor:"white",width:dimensions.fullWidth,height:(dimensions.fullHeight-75)}}> */}
           <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:(dimensions.fullHeight-75)}}>
            
            <FlatList
            refreshControl={
            <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
                
              />
            }
        data={this.state.serverData}
         ItemSeparatorComponent={this.renderSeparator}
         renderItem={({item,index}) => 
          
        <SwipeRow ref={(c) => { this.component[item.InvoiceID] = c }}
          rightOpenValue={ this.state.VATTYPE == item.VatType ? -200:-300} 
          backgroundColor={"orange"}
          onRowOpen={() => {
           if (this.selectedRow && this.selectedRow !== this.component[item.InvoiceID]) { this.selectedRow._root.closeRow(); }
           this.selectedRow = this.component[item.InvoiceID]
         }}
         right={
          this.state.VATTYPE == item.VatType ? <View style={{flexDirection:"row",flex:1}}>
           <TouchableOpacity onPress = {this.presspdf} style={{backgroundColor:"blue", flex: 1,
         alignItems: 'center',
         justifyContent: 'center',
         flexDirection: 'row'}}>
         <View><Image style={{height:30,width:30,tintColor:"white"}}  source = {require("./Images/newpdf.png")}/></View>
       </TouchableOpacity>
       <TouchableOpacity onPress = {this.pressdel} style={{backgroundColor:"red", flex: 1,
       alignItems: 'center',
       justifyContent: 'center',
       flexDirection: 'row'}}>
       <View><Image style={{height:30,width:30,tintColor:"white"}}  source = {require("./Images/delete.png")}/></View>
       </TouchableOpacity>
       
       <TouchableOpacity onPress = {this.presscopy} style={{backgroundColor:"lightseagreen", flex: 1,
         alignItems: 'center',
         justifyContent: 'center',
         flexDirection: 'row'}}>
         <View><Image style={{height:30,width:30,tintColor:"white"}}  source = {require("./Images/copy.png")}/></View>
       </TouchableOpacity>
       </View>:<View style={{flexDirection:"row",flex:1,backgroundColor:"#aaaaaa",justifyContent:"center",alignItems:"center"}}>
           
       <Text style={{fontFamily: 'Verdana',fontSize:16,fontWeight:"normal",color:"white",justifyContent:"center",textAlign:"center"}}>Please Change your VAT Reg. Type</Text>
       </View>}
        
        body={
          <TouchableOpacity onPress={() => this.invoicedetail(item)} style={styles.flatview}>
          <View style={[styles.randomcontainer, { backgroundColor: this._getRandomColor()}]}><Text style={{fontSize:20,fontWeight:"300",color:"white"}}>{this.ConvertTextToUpperCase(item)}</Text></View>
            <View style={{backgroundColor:"white" ,marginLeft:5,width:(dimensions.fullWidth-20)/2+25, }}>
            <Text style={{fontFamily: 'Verdana',fontSize:16,fontWeight:"bold",color:"black"}}>{item.InvoiceNumber}</Text>
            <View style={{backgroundColor:"white",flexDirection:"row",paddingTop:10}}>
            <Text style={{color:"grey",fontSize:14}}>Invoice Date:</Text>
            <Text numberOfLines={2} style={{color:"black",fontSize:14,fontWeight:"bold",}}>{item.InvoiceDate}</Text>

            </View>
            </View>
            <View style={{backgroundColor:"white",width:(dimensions.fullWidth-20)/4+5,justifyContent:"center",alignItems:"center"}}>
            <View style={{backgroundColor: item.Status  === "1" ?  '#2D6BA0' :  '#2685E1' ,width:(dimensions.fullWidth-20)/4,justifyContent:"center",alignItems:"center",borderRadius:5,height:30}}><Text style={{fontSize:15,fontWeight:"bold",color:"white"}}>{this.fun(item)}</Text></View>
            <View style={{backgroundColor:"white",width:(dimensions.fullWidth-20)/4,justifyContent:"center",alignItems:"center",height:30}}><Text ellipsizeMode={"tail"} numberOfLines={1} style={{color:"#128C0D",marginTop:5}}>£ {this.getvalue(item.InvoiceTotal)}</Text></View></View>
            </TouchableOpacity>
       }>
       
          
          </SwipeRow>}
  
            

        //onMomentumScrollBegin={this.loadMoreData} 
        keyExtractor={item => item.email}
        ListFooterComponent={this.empty()}
       // extraData={this.state.serverData}
        
       
       />
  
 
       <TouchableOpacity onPress={() => this.props.navigation.navigate("newinvoices")}  activeOpacity={0.5}  style={{backgroundColor:"#2685E1",width:60,height:60,borderRadius:30,bottom:(dimensions.fullHeight-20)/4-130,left:(dimensions.fullWidth)/2+90,justifyContent:"center",alignItems:"center",position:"absolute"}}>
       <Text style={{color:"white",fontSize:30,fontWeight:"normal"}}>+</Text></TouchableOpacity>
       {/* </TouchableOpacity> */}
       </View> 
      </View>
     
       </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  
  headerText: {
    fontSize: 15,
    textAlign: "center",
    margin: 10,
     backgroundColor:"orange",
    fontWeight: "bold"
  },
  flatview: {
     height:50,
    justifyContent: 'center',
    paddingTop: 0,
    borderRadius: 1,
    alignItems:"center",
    flexDirection:"row",
    backgroundColor:"white"
  },
  randomcontainer:{
    width:55,height:55,borderRadius:27.5,marginLeft:5,justifyContent:"center",alignItems:"center"
  },
  item: {
    padding: 10,
    fontSize: 20,
    height: 45,
    backgroundColor:"red"
  },
 
 
loadMoreBtn: {
  padding: 10,
  backgroundColor: '#2685E1',
  width:120,
  marginTop:10, 
  borderRadius: 4,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
},
btnText: {
  color: 'white',
  fontSize: 15,
  textAlign: 'center',
},
});
