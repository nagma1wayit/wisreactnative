import React, { Component } from 'react';
import {NetInfo,AsyncStorage,ActivityIndicator, View,Text,SafeAreaView,Dimensions,Alert ,Image,TouchableOpacity,TextInput,KeyboardAvoidingView,Platform} from 'react-native';
// import { TextInput } from 'react-native-gesture-handler';
var DeviceInfo = require('react-native-device-info-2');

const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }


export default class  login extends Component {

  //kapil1wayit@gmail.com
  //Cash@123
  constructor(props) {
    super(props);
    this.state = {
      email:"",
      password:"",
      deviceType : "",
      deviceID:"",
      device_Token:"",
      deviceIds:"",
      isLoading: false,
    };
  }
  componentDidMount(){
    //kapil-wis@yopmail.com  
        
    this.setState({isLoad:true});

    if (Platform.OS === "ios"){
 
      this.setState({deviceType : "ios"});
      console.log("Here is device Type : ",this.state.deviceType)

    }else{

      this.setState({deviceType : "android"});
      console.log("Here is device Type : ",this.state.deviceType)

    }


    this.setState({deviceID : DeviceInfo.getUniqueID()});
    
    AsyncStorage.setItem('Idsss',  DeviceInfo.getUniqueID())
    
    // this.state({deviceID:deviceids})



  }


loginAction(){

   console.log("Here is device Type : ",this.state.deviceType)
   console.log("Here is device ID : ",this.state.deviceID)
   
if (this.state.email.length == 0){

  Alert.alert("Enter username")

}else if (this.state.password.length == 0){
  
  Alert.alert("Enter password")

}else{

  NetInfo.isConnected.fetch().then(isConnected => {
    console.log("connected",isConnected)
    if(isConnected)
    {
        console.log('Internet is connected');
        this.setState({isLoading:true});
        setTimeout( () => {
          this.loginFunction()
        },3000);
      

    }else{

      Alert.alert("Check your internet connection");
    }
})


}



}
// https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/pushHistory/3125
  async  loginFunction() {

       var data = {
        uname: this.state.email,
        password: this.state.password,
        deviceId:this.state.deviceID,
        registerID:this.state.deviceID,
        deviceType : this.state.deviceType

      };
      console.log("parameters",this.state.deviceID);
      
      try {
       let response = await fetch(
        "https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/login",
        {
          method: "POST",
          headers: {
           "Accept": "application/json",
           "Content-Type": "application/json"
          },
         body: JSON.stringify(data)
        
       }
      );
      console.log("ghsghjhjku",response)

       if (response.status >= 200 && response.status < 300) {
         this.setState({isLoading:false})
        const myArrStr = response._bodyInit;
        const newData = JSON.parse(myArrStr)
        console.log(newData);
        // const newData1 = newData.token
        const userid = newData.ID
        const username = newData.Name
        const useremail = newData.Email
        const vatno = newData.VatRegNo
        const companyyear = newData.companyFiledYear
        const compreg = newData.CompanyRegNo
        const apikey = newData.ApiKey
        const cids = newData.Companies[0].CID
         console.log("api",apikey);
         console.log("cddddd",cids);
        // console.log("name",username);
        // console.log("id",userid);
        // console.log("email",useremail);
        // console.log("cmp",company)
        
       
        AsyncStorage.setItem("uid",userid);
        AsyncStorage.setItem("uemail",useremail);
        AsyncStorage.setItem("uname",username);
        AsyncStorage.setItem("vno",vatno);
        AsyncStorage.setItem("cyear",companyyear);
        AsyncStorage.setItem("compregno",compreg);
        AsyncStorage.setItem("apk",apikey);
        AsyncStorage.setItem("companycid",cids);
        

        this.setState({email:'',password:''})
         this.props.navigation.navigate('companyscreen')
        
         //this.props.navigation.navigate("companyscreen")
  
      }else {
         
        this.setState({isLoading:false})
        this.setState({email:'',password:''})
        Alert.alert("Alert","Invalid email or password!" );
  
      }
     } catch (errors) {
  
      this.setState({isLoading:false})

        Alert.alert("Alert","Please Check your internet connection" );
    }
  }
  restrict(event){
    var re = /^[A-Za-z]+$/;
    if(re.test(event)){

      this.setState({email:event})
      return true
     }
    else{
      alert('Invalid Name.');      

      return false
    }
  }
  

  render() {
    if (this.state.isLoading) {
           
      return (
        <View style={{ flex: 1, paddingTop: 0,justifyContent:"center",alignSelf:"center" }}>
          <View style={{backgroundColor:"#F3F3F7",borderRadius:10,height:70,width:70,justifyContent:"center",alignItems:"center"}}><ActivityIndicator size="large" color="grey"/></View>
        </View>
      );
    } 

    return (
      <SafeAreaView style={{flex:1,alignItems: 'center',backgroundColor:"white" }}>
     
         <KeyboardAvoidingView behavior="padding" enabled style={{backgroundColor:"white",alignItems:"center",justifyContent:"center", width:dimensions.fullWidth,height:dimensions.fullHeight}}>
         <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight,backgroundColor:"white"}}>
 
      <View style={{width:250,height:120,marginTop:150,alignSelf:"center",alignItems:"center",backgroundColor:"white" }}>
         <Image resizeMode='contain' source = {require("./Images/newlogo.png")}style={{width:'100%',height:'100%',alignSelf:"center"}} />
         </View>
         <View style={{width:dimensions.fullWidth-30, height:150,marginLeft:15,marginTop:20,backgroundColor:"white" }}>
         <View style={{width:dimensions.fullWidth-40,height:50,marginLeft:5,alignSelf:"center",flexDirection:"row",borderBottomWidth:1,borderBottomColor:"#0034A8"}}> 
           <TextInput 
           autoCapitalize={false} 
           onChangeText={(text)=>this.setState({ email:text})}
           style={{width:dimensions.fullWidth-85,height:50,marginLeft:5,paddingTop:20,fontSize:15,fontWeight:"bold",}}
           placeholder="Enter User Name"
           placeholderTextColor="grey" />
              <Image source={require("./Images/user.png")}style={{width:20,height:26,top:18,marginLeft:1}}></Image> 
           </View>
           <View style={{ width:dimensions.fullWidth-40,height:50,marginLeft:5,marginTop:20,alignSelf:"center",flexDirection:"row",borderBottomWidth:1,borderBottomColor:"#0034A8"}}> 
           <TextInput 
           onChangeText={(text)=>this.setState({ password:text})}
           style={{width:dimensions.fullWidth-85 ,height:50,marginLeft:5,paddingTop:20,fontSize:15,fontWeight:"bold", }}  placeholder="Enter Password" secureTextEntry={true} placeholderTextColor="grey" />
            <Image source={require("./Images/lock.png")}style={{width:20,height:25,top:20,marginLeft:1}} />
           </View>
           
         
         </View>
         
         <TouchableOpacity  onPress={() => this.loginAction()}
                style={{width:dimensions.fullWidth-35,height:50,backgroundColor:"#0034A8",alignItems:"center",justifyContent:"center",marginLeft:20,top:20}}>
         <Text style={{color:"white",fontSize:18,fontWeight: 'bold'}}>Login</Text>
         </TouchableOpacity>
          
          
         </View>
         </KeyboardAvoidingView>
 
      </SafeAreaView>
    );
  }
}
