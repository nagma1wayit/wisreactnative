import React, { Component } from 'react';
import { View, Text,SafeAreaView,Dimensions,RefreshControl,AsyncStorage,ActivityIndicator,TouchableOpacity,StyleSheet,Image,FlatList,Alert} from 'react-native';
const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}
 
import { SwipeRow, Icon, Button } from 'native-base';
 
export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ColorHolder : "green",
      refreshing: false, 
      Id:"", 
      apk:'',
      datas: [],
     
  };
  this.component = [];
  this.selectedRow;
  }
  componentDidMount(){


   
    AsyncStorage.getItem("apk").then((value) => {
      
      this.setState({apk: value});
      console.log(this.state.apk)
      this.mileage()
    
  }).done();
   }
   mileage (){
    //http://docpoke.in/greenleafairupdate/api/getNotifyList?user_id=
    fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/expenses/0/MILEAGE/' + this.state.apk)
       .then((response) => response.json())
       .then((responseJson) => {
  
  
         console.log("data mileage:",responseJson)
         
           console.log("number:",responseJson[0].Expenses[0].ExpenseNumber)
          console.log("mnth:",responseJson[0].Expenses[0].Month)
          console.log("paid:",responseJson[0].Expenses[0].PaidOn)
          console.log("iyesarrr:",responseJson[0].Expenses[0].Year)
          console.log("statssss:",responseJson[0].Expenses[0].Status)
          console.log("iddssss:",responseJson[0].Expenses[0].ID)
         this.setState({
          datas:responseJson[0].Expenses
         })
         
         
       })
       .catch((error) =>{
         console.log(error);
       });
     }
  
   
   toggleDrawer1 = () => {
 
    console.log(this.props.navigation);
 
    this.props.navigation.toggleDrawer();
 
  }
  
  
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#CED0CE",
          marginTop:0
        }}
        />
    );
  };

  ConvertTextToUpperCase=(val)=>{
 

      var str = val.ExpenseNumber;
     var char =  str.charAt(0)
     return char;
    
 
  }
  _getRandomColor(){
     
    var ColorCode = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 268)) + ',' + (Math.floor(Math.random() * 278)) + ')';
 
 return ColorCode;
  }
  pressdel = () =>{
    {Alert.alert('Delete Mileage',
    'Are you sure you want to delete mileage ?',
    [
      {
        text: 'No',
       
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Yes',
         
        onPress: () => {this.deleteItemById()},
      },
    ],)}
  }
  presspdf = () =>{
    {Alert.alert('Pdf Download',
      'Are you sure you want to Download & Share Pdf ?',
      [
        {
          text: 'Cancel',
         
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Download & Share',
     
          onPress: () => console.log('ok Pressed'),
        },
      ],)}
  }
  presscopy = () =>{
    {Alert.alert('Copy Mileage',
      'Are you sure you want to copy mileage',
      [
        {
          text: 'Cancel',
         
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Ok',
          
          onPress: () => console.log('ok Pressed'),
        },
      ],)}
  }
  _delete(){
     const newState = this.state.datas.slice();
     if (newState.indexOf(item) > -1) {
     newState.splice(newState.indexOf(item), 1);
     this.setState({datas: newState})
    }
    
  }
  _onRefresh = () => {
    this.setState({refreshing: true});
    this._getRandomColor(),
    this.setState({refreshing: false});
   
  }
  empty = () => {
    return (
      <View style={{height:(dimensions.fullHeight)/5-40,width:dimensions.fullWidth,backgroundColor:"white"}}/>
    );
  };
  fun = (stat) => {
    if(stat.Status == "2"){
    var crt = "CREATED"
    var status1 = crt;
     //console.log(status1)
     return status1;
    }
    else if (stat.Status == "1"){
      var crts = "DRAFT";
      var status2 = crts;
      return status2;
    //  console.log("draft")
     
   } 
    else{
      console.log("ok")
    }
     
  }
  months = (mt) =>{
    if(mt.Month == "1"){
      var mon = "January"
      var month1 = mon;
       //console.log(status1)
       return month1;
      }
      else if(mt.Month == "2"){
        var feb = "February"
        var month2 = feb;
         //console.log(status1)
         return month2;
        }
        else if(mt.Month == "3"){
          var Mar = "March"
          var month3 = Mar;
           //console.log(status1)
           return month3;
          }
          else if(mt.Month == "4"){
            var Apr = "April"
            var month4 = Apr;
             //console.log(status1)
             return month4;
            }
            else if(mt.Month == "5"){
              var May = "May"
              var month5 = May;
               //console.log(status1)
               return month5;
              }
              else if(mt.Month == "6"){
                var Jun = "June"
                var month6 = Jun;
                 //console.log(status1)
                 return month6;
                }
                else if(mt.Month == "7"){
                  var Jul = "July"
                  var month7 = Jul;
                   //console.log(status1)
                   return month7;
                  }
                  else if(mt.Month == "8"){
                    var Aug = "August"
                    var month8 = Aug;
                     //console.log(status1)
                     return month8;
                    }
                    else if(mt.Month == "9"){
                      var Sep = "September"
                      var month9 = Sep;
                       //console.log(status1)
                       return month9;
                      }
                      else if(mt.Month == "10"){
                        var Oct = "October"
                        var month10 = Oct;
                         //console.log(status1)
                         return month10;
                        }
                        else if(mt.Month == "11"){
                          var Nov = "November"
                          var month11 = Nov;
                           //console.log(status1)
                           return month11;
                          }
                          else if(mt.Month == "12"){
                            var Dec = "December"
                            var month12 = Dec;
                             //console.log(status1)
                             return month12;
                            }
      else{
        console.log("ok")
      }
  }
   
  getvalue = (amount) => { 

    var Tamount = amount.TotalVATAmount;
    console.log(Tamount)
    var Damount = amount.diffTotalAmount
    console.log(Damount)
     var Total = parseFloat(Tamount+Damount);
     console.log(Total)

    if(Total == 0){
       
       return "0.00"
       
    }else{ 
  
      var value = 0;
      value = parseFloat(Total).toFixed(2);
      console.log("iteprice",value)
     return value
  
    } 
     
  }  
    
  render() {
   
    return (
      <SafeAreaView style={{backgroundColor:"white",flex:1}}>
      <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"pink",marginTop:0}}>
      <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
       <TouchableOpacity onPress={this.toggleDrawer1}  style={{width:50, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
       <Image source={require("./Images/menu.png")}  style={{height:20,width:20}}/>
       </TouchableOpacity>
       <Text style={{ width:dimensions.fullWidth-125,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>MILEAGE</Text>
       </View> 
      
       {/* <TouchableOpacity style={{backgroundColor:"white",width:dimensions.fullWidth,height:(dimensions.fullHeight-75)}}> */}
           <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:(dimensions.fullHeight-75)}}>
            <FlatList
            refreshControl={
            <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
        data={this.state.datas}
         ItemSeparatorComponent={this.renderSeparator}
         renderItem={({item}) => 
          
          <SwipeRow ref={(c) => { this.component[item.name,item.date] = c }}
          rightOpenValue={-200}
          backgroundColor={"orange"}
          onRowOpen={() => {
           if (this.selectedRow && this.selectedRow !== this.component[item.name,item.date]) { this.selectedRow._root.closeRow(); }
           this.selectedRow = this.component[item.name,item.date]
         }}
         right={
         <View style={{flexDirection:"row",flex:1}}>
           <TouchableOpacity onPress = {this.presspdf} style={{backgroundColor:"blue", flex: 1,
         alignItems: 'center',
         justifyContent: 'center',
         flexDirection: 'row'}}>
         <View><Image style={{height:30,width:30,tintColor:"white"}}  source = {require("./Images/newpdf.png")}/></View>
       </TouchableOpacity>
       <TouchableOpacity onPress = {this.pressdel} style={{backgroundColor:"red", flex: 1,
       alignItems: 'center',
       justifyContent: 'center',
       flexDirection: 'row'}}>
       <View><Image style={{height:30,width:30,tintColor:"white"}}  source = {require("./Images/delete.png")}/></View>
       </TouchableOpacity>
       
       <TouchableOpacity onPress = {this.presscopy} style={{backgroundColor:"lightseagreen", flex: 1,
         alignItems: 'center',
         justifyContent: 'center',
         flexDirection: 'row'}}>
         <View><Image style={{height:30,width:30,tintColor:"white"}}  source = {require("./Images/copy.png")}/></View>
       </TouchableOpacity>
       </View>}
        
        body={
          <TouchableOpacity style={styles.flatview}>
          <View style={[styles.randomcontainer, { backgroundColor: this._getRandomColor()}]}><Text style={{fontSize:20,fontWeight:"300",color:"white"}}>{this.ConvertTextToUpperCase(item)}</Text></View>
            <View style={{backgroundColor:"white" ,marginLeft:5,width:(dimensions.fullWidth-20)/2+25, }}>
            <Text style={{fontFamily: 'Verdana',fontSize:16,fontWeight:"bold",color:"black"}}>{item.ExpenseNumber}</Text>
            <View style={{backgroundColor:"white",flexDirection:"row",paddingTop:10}}>
            <Text style={{color:"grey",fontSize:14}}>Mileage Date:</Text>
            <Text numberOfLines={2} style={{color:"black",fontSize:14,fontWeight:"bold",paddingLeft:5,}}>{this.months(item)} {item.Year}</Text>

            </View>
            </View>
            <View style={{backgroundColor:"white",width:(dimensions.fullWidth-20)/4+5,justifyContent:"center",alignItems:"center"}}>
            <View style={{backgroundColor:"#2685E1" ,width:(dimensions.fullWidth-20)/4,justifyContent:"center",alignItems:"center",borderRadius:5,height:30}}><Text style={{fontSize:15,fontWeight:"bold",color:"white"}}>{this.fun(item)}</Text></View>
            <Text style={{color:"#128C0D",marginTop:5}}>£ {this.getvalue(item)}</Text></View>
            </TouchableOpacity>
       }>
        
          
          </SwipeRow>}
  
            

        
        keyExtractor={item => item.email}
        ListFooterComponent={this.empty()}
       />
 
 
       <TouchableOpacity onPress={() => this.props.navigation.navigate("newmileages")} activeOpacity={0.5}  style={{backgroundColor:"#2685E1",width:60,height:60,borderRadius:30,bottom:(dimensions.fullHeight-20)/4-130,left:(dimensions.fullWidth)/2+90,justifyContent:"center",alignItems:"center",position:"absolute"}}>
       <Text style={{color:"white",fontSize:30,fontWeight:"normal"}}>+</Text></TouchableOpacity>
       {/* </TouchableOpacity> */}
       </View> 
      </View>
       </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  
  headerText: {
    fontSize: 15,
    textAlign: "center",
    margin: 10,
     backgroundColor:"orange",
    fontWeight: "bold"
  },
  flatview: {
     height:50,
    justifyContent: 'center',
    paddingTop: 0,
    borderRadius: 1,
    alignItems:"center",
    flexDirection:"row",
    backgroundColor:"white"
  },
  randomcontainer:{
    width:55,height:55,borderRadius:27.5,marginLeft:5,justifyContent:"center",alignItems:"center"
  },
  item: {
    padding: 10,
    fontSize: 20,
    height: 45,
    backgroundColor:"red"
  }
});
