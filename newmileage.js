import React, { Component } from 'react'
import { Text, View,SafeAreaView,TouchableOpacity,AsyncStorage,Image,ScrollView,StyleSheet,Dimensions,FlatList} from 'react-native'
import { SwipeRow, Icon, Button, Content, Container } from 'native-base';
 
const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }
  export default class newmileage extends Component {
    constructor(props) {
      super(props);
      this.state = {
        name1:"",
        name2:"",
        name3:"Select Employee",
       refreshing: false,
       data: [ { "name": "TES-20190212-37251", "date": "12-02-2019" },
       {"name": "Ebony Maw","date": "13-02-2019"    },
       {"name": "Black Dwarf","date": "14-02-2019"  },
       {"name": "Mad Titan","date": "15-02-2019"    },
       {"name": "Mad Titan","date": "16-02-2019"    },
       {"name": "Mad Titan","date": "17-02-2019"    },
       {"name": "Mad Titan","date": "18-02-2019"    },
       {"name": "Mad Titan","date": "19-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Mad Titan","date": "20-02-2019"    },
       {"name": "Supergiant","date": "21-02-2019"   },
       { "name": "TES-20190212-37251","date": "12-02-2019"},
       { "name": "TES-20190212-37251","date": "12-02-2019"},
       { "name": "TES-20190212-37251","date": "12-02-2019"},],
      };
      this.component = [];
      this.selectedRow;
    }
   


    componentWillReceiveProps(item1,item2,item3) {

      AsyncStorage.getItem("name1").then((value) => {
        
        this.setState({name1: value});
        
       
     }).done();
     AsyncStorage.getItem("name2").then((val) => {
      
      this.setState({name2: val});
    }).done();
    AsyncStorage.getItem("name3").then((vals) => {
      
      this.setState({name3: vals});
    }).done();
   }

   componentDidMount() {
    const monthName = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  
       const d = new Date(); 
       this.setState({name1:monthName[d.getMonth()]})
        var yearr = new Date().getFullYear();

        this.setState({name2:yearr + ''})
        
     
    AsyncStorage.setItem('name1',monthName[d.getMonth()]);
    AsyncStorage.setItem('name2', yearr + '');
    AsyncStorage.setItem('name3', "Select Employee");
   }
   refresh(){


   }
 
 refresh(){


 }
 
    
    gobackAction=()=>{

        this.props.navigation.goBack()
            
             
        }
        empty = () => {
          return (
            <View style={{height:50,width:dimensions.fullWidth,backgroundColor:"#EFEFF4"}}/>
          );
        };
        
  render() {

 

    return (
        <SafeAreaView  style={{backgroundColor:"white",flex:1}}>
        <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"white",marginTop:0}}>
       <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
        <TouchableOpacity onPress = {this.gobackAction}  style={{ width:40, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
        <Image source={require("./Images/leftarrow.png")}  style={{height:20,width:25,tintColor:"white"}}/>
        </TouchableOpacity>
        <Text style={{width:dimensions.fullWidth-80,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>NEW MILEAGE</Text>
        </View>
        <View style={{width:dimensions.fullWidth,height:(dimensions.fullHeight-220),backgroundColor:"#EFEFF4"}}> 
        <ScrollView>        
        <TouchableOpacity  onPress={() => this.props.navigation.navigate("empmileage")} style={{backgroundColor:"white",borderBottomWidth:1,borderBottomColor:"grey",width:dimensions.fullWidth,height:60,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth-10)/2-20,marginLeft:5,fontWeight:"bold", fontSize:16,color:"#9A9A9A"}}>Employee Name</Text><Text style={{width:(dimensions.fullWidth-10)/2, fontSize:16,color: this.state.name3 === "Select Employee" ?  '#9A9A9A' :  'black' ,fontWeight:"bold", textAlign:"right"}}>{this.state.name3}</Text><Image source={require("./Images/rightarrow.png")}  style={{height:15,width:15,tintColor:"#9A9A9A"}}/></TouchableOpacity>
        <TouchableOpacity  onPress={() => this.props.navigation.navigate("monthmileage")} style={{backgroundColor:"white",borderBottomWidth:1,borderBottomColor:"grey",width:dimensions.fullWidth,height:60,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth-10)/2-20,marginLeft:5,fontWeight:"bold", fontSize:16,color:"#9A9A9A"}}>Month</Text><Text style={{width:(dimensions.fullWidth-10)/2, fontSize:16,color: this.state.name1 === "" ?  '#9A9A9A' :  'black' ,fontWeight:"bold",textAlign:"right"}}>{this.state.name1}</Text><Image source={require("./Images/rightarrow.png")}  style={{height:15,width:15,tintColor:"#9A9A9A"}}/></TouchableOpacity>
        <TouchableOpacity  onPress={() => this.props.navigation.navigate("yearmileage")} style={{backgroundColor:"white",borderBottomWidth:1,borderBottomColor:"grey",width:dimensions.fullWidth,height:60,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth-10)/2-20,marginLeft:5,fontWeight:"bold", fontSize:16,color:"#9A9A9A"}}>Year</Text><Text style={{width:(dimensions.fullWidth-10)/2, fontSize:16,color: this.state.name2 === "Select Year" ?  '#9A9A9A' :  'black' ,fontWeight:"bold", textAlign:"right"}}>{this.state.name2}</Text><Image source={require("./Images/rightarrow.png")}  style={{height:15,width:15,tintColor:"#9A9A9A"}}/> 
      </TouchableOpacity>
      <View style={{backgroundColor:"purple",width:dimensions.fullWidth}}>
     
    <FlatList
    
  scrollEnabled={false}
  ListFooterComponent={this.empty()}
  data={this.state.data}
  ItemSeparatorComponent={this.renderSeparator}
  renderItem={({item,index}) => <SwipeRow ref={(c) => { this.component[item,index] = c }}
               rightOpenValue={-75}
               onRowOpen={() => {
                if (this.selectedRow && this.selectedRow !== this.component[item,index]) { this.selectedRow._root.closeRow(); }
                this.selectedRow = this.component[item,index]
              }}
              right={
                <TouchableOpacity onPress = {this.press} style={{backgroundColor:"#2685FF", flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',}}>
                <View>
              <Image style={{height:30,width:30,tintColor:"white"}}  source = {require("./Images/delete.png")}/>
            </View>
            </TouchableOpacity>
              }
              body={
                //  <TouchableOpacity style={styles.flatview}>
     <View  style={{backgroundColor:"white",width:(dimensions.fullWidth-10),height:30,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
     <Text style={{width:(dimensions.fullWidth-10)/2-5,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>{item.name}</Text>
     
      <Text style={{color:"black", width:(dimensions.fullWidth-10)/2-5,textAlign:"right",fontSize:16,fontWeight:"bold",color:"black"}}>{item.date}</Text>
     </View>
      // </TouchableOpacity>
              }>
  </SwipeRow>}
  
  
/> 
</View>
        </ScrollView>
        </View>
        
        <View style={{borderBottomWidth:1,borderBottomColor:"grey",width:dimensions.fullWidth}}></View>
        <TouchableOpacity  onPress={() => this.props.navigation.navigate("addmileage")} style={{backgroundColor:"white",borderBottomWidth:1,borderBottomColor:"grey",width:dimensions.fullWidth,height:60,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><View style={{width:20,height:20,alignItems:"center",justifyContent:"center"}}><Image source={require("./Images/adt.png")}  style={{height:20,width:20,tintColor:"#2685FF"}}/></View><Text style={{fontSize:16,color:"#2685FF",marginLeft:10,fontWeight:"bold", }}>Add Mileage Items</Text></TouchableOpacity>
        <View style={{backgroundColor:"white",width:(dimensions.fullWidth),height:80,justifyContent:"center",flexDirection:"row",}}>
        <TouchableOpacity style={{width:(dimensions.fullWidth)/2-25,marginTop:5, marginLeft:0,borderRadius:5,height:40,backgroundColor:"#0034A8",alignItems:"center",justifyContent:"center",}}><Text style={{color:"white",fontSize:15,fontWeight:"bold"}}>Draft</Text></TouchableOpacity>
        <TouchableOpacity style={{width:(dimensions.fullWidth)/2-25,marginTop:5,  marginLeft:20,borderRadius:5,height:40,backgroundColor:"#0034A8",justifyContent:"center",alignItems:"center", }}><Text style={{color:"white",fontSize:15,fontWeight:"bold"}}>Create</Text></TouchableOpacity>
        </View>
        
        </View>
        </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  
  
    flatview: {
      height:60,
      justifyContent: 'center',
      borderRadius: 1,
      alignItems:"center",
      flexDirection:"row",
      backgroundColor:"red"
    },
     
     
     
    item: {
      padding: 10,
      fontSize: 20,
      height: 30,
      backgroundColor:"blue"
    }
  });