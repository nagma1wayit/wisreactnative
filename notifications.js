import React, { Component } from 'react';
import { View, Text, SafeAreaView, Dimensions,StyleSheet,RefreshControl, Alert,FlatList,  TouchableOpacity,Image} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
const dimensions = {
 fullHeight: Dimensions.get('window').height,
 fullWidth: Dimensions.get('window').width
}

class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
     refreshing: false,
     userId:"",
     dataSource: [],
    };
  }

  componentDidMount(){


   this.notification()

  }

  notification (){

  fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/pushHistory/userId')
     .then((response) => response.json())
     .then((responseJson) => {


       console.log("data is here:",responseJson)
       
        console.log("eventdate:",responseJson[0].eventDate)
        console.log("eventdate:",responseJson[0].eventType)
        
       this.setState({
         dataSource:responseJson
       })
       
       
     })
     .catch((error) =>{
       console.log(error);
     });
   }


  empty = () => {
   return (
     <View style={{height:(dimensions.fullHeight)/5-60,width:dimensions.fullWidth,backgroundColor:"white"}}/>
   );
 };
  toggleDrawer1 = () => {

   this.props.navigation.toggleDrawer();

 }
 renderSeparator = () => {
   return (
     <View
       style={{
         height: 1,
         width: "100%",
         backgroundColor: "#CED0CE",
         marginTop:0
       }}
       />
   );
 };
 
 _onRefresh = () => {
   this.setState({refreshing: true});
   // // this._getRandomColor(),
   this.setState({refreshing: false});
  
 }

  showalert = (eventType) => {
    Alert.alert('Message Detail',eventType,[
     
     {
       text: 'Dismiss',
       
       onPress: () => console.log('Dismiss Pressed'),
     },
   ])
  }
  render() {
    return (
     <SafeAreaView>
     <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,marginTop:0}}>
     <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
    <TouchableOpacity onPress={this.toggleDrawer1}  style={{width:50, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
    <Image source={require("./Images/menu.png")}  style={{height:20,width:20}}/>
    </TouchableOpacity>
    <Text style={{ width:dimensions.fullWidth-125,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>Notifications</Text>
    </View> 
    <View style={{backgroundColor:"#EFEFF4",width:dimensions.fullWidth,height:50,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
   <Text style={{width:(dimensions.fullWidth)/2-40,paddingLeft:20, fontSize:18,color:"black",fontWeight:"bold"}}>Date</Text>
   <Text style={{width:(dimensions.fullWidth)/2+40,textAlign:"center", fontSize:18,color:"black",fontWeight:"bold"}}>Message</Text>
    </View>
    <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:(dimensions.fullHeight-75)}}>
    <FlatList 
     refreshControl={
       <RefreshControl
           refreshing={this.state.refreshing}
           onRefresh={this._onRefresh}
         />
       }  
       ListFooterComponent={this.empty()}
       data={this.state.dataSource}
        ItemSeparatorComponent={this.renderSeparator}
     renderItem={({item}) => <TouchableOpacity onPress = {() => this.showalert(item.eventType)}  style={styles.flatview}>
    <View style={{backgroundColor:"white",width:(dimensions.fullWidth)/2-40,paddingLeft:10,height:45,justifyContent:"center",alignItems:"center"}}><Text style={{width:(dimensions.fullWidth)/2-40,marginLeft:10,fontSize:18,color:"black",fontWeight:"normal"}}>{item.eventDate}</Text>
</View>
    <View style={{height:50,backgroundColor:"#CED0CE",width:1}}></View>
    <View style={{backgroundColor:"white",width:(dimensions.fullWidth)/2+40,paddingLeft:10,height:45,justifyContent:"center",alignItems:"center"}}><Text numberOfLines={1} style={{width:(dimensions.fullWidth)/2+40,marginLeft:10,fontSize:18,color:"black",fontWeight:"normal"}}>{item.eventType}</Text></View>
     </TouchableOpacity>}
       />
       </View>
    
    </View> 
  
      </SafeAreaView>
    );
  }
}

export default componentName;

const styles = StyleSheet.create({
  row:{
    flexDirection : "row",
    backgroundColor:"red",
    marginTop:10
  },
  flatview: {
   height:50,
  justifyContent: 'center',
  paddingTop: 0,
  borderRadius: 1,
  alignItems:"center",
  flexDirection:"row",
  backgroundColor:"white"
},
item: {
 padding: 10,
 fontSize: 20,
 height: 45,
 backgroundColor:"red"
},
  row2:{
   flexDirection : "row"
  
 },
 bold :{
   fontWeight :'bold'
 },
emptyWidth1: {
   width:'5%',
   backgroundColor:"green"
 },
 emptyWidth2: {
   width:'5%'
 },
 textWidth1: {
   width:'45%'
 },
 emptyWidth3: {
   width:'5%',
   borderBottomColor:'#8395a7',
   borderBottomWidth:1
 },
 textWidth3: {
   width:'30%',
   borderRightWidth:1,
   borderBottomWidth:1,
   height:55,
   justifyContent :'center',
   alignItems:'center',
   borderBottomColor:'#8395a7',
   borderRightColor:'#8395a7'
   
 },
 textWidth4: {
   width:'70%',
   justifyContent :'center',
   borderBottomColor:'#8395a7',
   borderBottomWidth:1
 },
 textWidth2: {
   width:'45%'
 },
 centerText :{
   fontSize:18
 },
 centerText2 :{
  fontSize:24
 }
})
// import React, { Component } from 'react';
// import { View, Text ,ScrollView,SafeAreaView,Dimensions,AsyncStorage,TouchableOpacity,Alert,StyleSheet,FlatList,Image} from 'react-native';
// import { SwipeRow, Icon, Button } from 'native-base';
// import DateTimePicker from 'react-native-modal-datetime-picker';
// import moment, { min } from 'moment';
// import items from './items';
// import {NavigationEvents} from 'react-navigation';
// const dimensions = {
//     fullHeight: Dimensions.get('window').height,
//     fullWidth: Dimensions.get('window').width
//   }
// export default class newinvoice extends Component {
//   constructor(props) {
    
//     super(props);
//     Obje = new items();
//      this.state = {
//       custom:"Select", 
//       dateset:"Select Date",
//       datesets:"Select Due Date",
//       status : false,
//       isDateTimePickerVisible: false,
//       DateTimePickerVisible: false, 
//       allamount:"0.00",
//       subtotal:"0.00",
//       Vatpercen:"0.00",  
//       dataitem:[],
//       // data1:[], 
//       subtarray:[],
//       vatsarray:[],
//       newarray:[],
//       vatper:"",
//       apk:'',
//       itembject:"",
//       idcustomer:"",
//       responseAPI:"",
//       datarecieves:"",
//       alremain:"",
//       indexreplace:"",
//       newitem:"",
//       flats:[]
//     };
//     this.component = [];
//     this.selectedRow;
//   }
  
 
//  componentDidMount(vatvalue) {
//   AsyncStorage.getItem("apk").then((value) => {
      
//     this.setState({apk: value});
//     console.log("aps",this.state.apk)
   
  
// }).done();
// AsyncStorage.getItem("vatper").then((value) => { 
//   this.setState({vatper: value});
//   console.log("vdshshsfkfdfdsfd",this.state.vatper)
 
// }).done();
//  this.refresh()
// AsyncStorage.setItem('custom',  "Select"); 
// AsyncStorage.setItem('dataitem',  "");
// //AsyncStorage.setItem('idexid',  "0");
       
// if(this.props.navigation.state.params){
 
//   if(this.props.navigation.state.params.remaindata){ 
//     AsyncStorage.getItem("apk").then((value) => {
      
//       this.setState({apk: value});
//       console.log("aps",this.state.apk)
//       const { navigation } = this.props;
//       const recev = navigation.getParam('remaindata');
//       console.log("recieve",recev)
//       this.setState({datarecieves:recev.InvoiceID, alremain:recev});
//       this.invoicedraft();
    
//   }).done();
  
// } }
  

 
//  }
  

//  invoicedraft(){
  
//   console.log("eve",this.state.datarecieves)
//   console.log("hhj",this.state.apk) 
//   fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/invoice/itemId/' + this.state.datarecieves + '/API-KEY/' + this.state.apk)
//   .then((response) => response.json())
//      .then((responseJson) => {


//        console.log("invoice draft:",responseJson)
       
//           var namies=responseJson[0].Invoice.Name 
//           var invoicdate=responseJson[0].Invoice.InvoiceDate
//           var dudate=responseJson[0].Invoice.DueDate
           
//           this.setState({newitem:responseJson[0].Invoice.InvoiceItems})
          
//           var newtotal=responseJson[0].Invoice.InvoiceItems[0].Quantity
//           console.log("changes",this.state.newitem)
//           console.log("chan",newtotal)

//           this.setState({custom:namies})
//           this.setState({idcustomer:responseJson[0].Invoice.UserID})
//           this.setState({dateset:invoicdate})
//           this.setState({datesets:dudate})
//           this.setState({dataitem:this.state.newitem})
//           this.setState({subtotal:this.state.alremain.SubTotal})
//           this.setState({Vatpercen:this.state.alremain.Tax})
//           this.setState({allamount:this.state.alremain.InvoiceTotal})
//           // var Quant= responseJson[0].Invoice.InvoiceItems[0].Quantity
//           // var price= responseJson[0].Invoice.InvoiceItems[0].UnitPrice
          
           
//       //  this.setState({
//       //   data:responseJson[0].Invoice,
//       //   data1:responseJson[0].Invoice.InvoiceItems[0],
        
//       //  })
       
       
//      })
//      .catch((error) =>{
//        console.log(error);
//      });
//    }








//  event = () => { 
    
//   AsyncStorage.getItem("custom").then((value) => {
//    console.log(value, 'value')
//     if (value == "Select"){

//     }else {
//     var obj=JSON.parse(value)
//     console.log("complete",obj)
//     if(obj.names){
//     console.log("object",obj.names) 
//     console.log("object",obj.listid)
    
//     this.setState({idcustomer:obj.listid})
//     this.setState({custom: obj.names });
//   }else{
//     console.log("ivety",obj[0].names) 
     
//     this.setState({custom: obj[0].names });
//   }
   
    
//     }
    
   
//  }).done();

   
   
    
// AsyncStorage.getItem('dataitem').then((value) => { 

//   var obj=JSON.parse(value);
//   console.log("jsonobj",obj)
   
//  if(obj != null  ) { 
// this.setState({dataitem:obj})
// console.log("nagma",this.state.dataitem)
 
  
//  }
   
//   if (this.props.navigation.state.params )
//    { 
//     if(this.state.dataitem[this.props.navigation.state.params.myObj.itemid]){

    
//    console.log("patha",this.state.dataitem[this.props.navigation.state.params.myObj.itemid]);
//    console.log("dataitmnew",this.state.dataitem)
//    this.state.dataitem[this.props.navigation.state.params.myObj.itemid] = this.props.navigation.state.params.myObj
//    console.log("dataitmnew2",this.state.dataitem)
//    this.setState({dataitem:this.state.dataitem})
//    AsyncStorage.setItem('dataitem', JSON.stringify(this.state.dataitem))
//    this.subt()
//   // this.props.navigation.navigate('newinvoices', {"myObj" :this.state.array}) 

//   }
//      if(this.props.navigation.state.params.myObj[0].myobj !== "back" )
//     // if(this.props.navigation.state.params.myObj[0])

//      {
//      console.log("lall",this.props.navigation.state.params)
    
//     this.state.dataitem.push(this.props.navigation.state.params.myObj[0].myobj); 
//     console.log(this.state.dataitem.length, 'dataitem')
//     if(this.state.dataitem.length == 1)
//     {
      
//       this.setState({dataitem: this.state.dataitem} );
//       console.log("pinku",this.state.dataitem)
//     } 
//   AsyncStorage.setItem('dataitem',  JSON.stringify(this.state.dataitem)); 
//   this.subt()
//    this.props.navigation.navigate('newinvoices', {"myObj" : this.state.array}) 

//   }
//   } 
// }) 
//  }

//  subt(){
   
//    var subarray=[]

//   for(var i =0;i<this.state.dataitem.length;i++){
//     //subarray.push(this.state.dataitem[i].stotal)
//     subarray.push(this.state.dataitem[i].Quantity * this.state.dataitem[i].UnitPrice)

//   console.log("pin",subarray)
      
    
//   }
//   this.setState({subtarray:subarray})
//   console.log("hujh",this.state.subtarray) 

//     addarray= 0
//   for(var i =0 ;i<this.state.subtarray.length;i++){
//     addarray += this.state.subtarray[i];
//   }
  
//    this.setState({subtotal:addarray})
//    console.log("vl",this.state.subtotal)
//   this.newvat(this.state.subtotal)
//  }



//  newvat(st){
// console.log("t",st)
// console.log("tim",this.state.vatper)
// var vatadd= st * (this.state.vatper)/100
// this.setState({Vatpercen:vatadd})
// console.log("cher",this.state.Vatpercen)
// var vatsubt = st + this.state.Vatpercen
// this.setState({allamount:vatsubt})
// console.log("vatsat",this.state.allamount)
   
// //   var vatarray=[]

// //  for(var i =0;i<this.state.dataitem.length;i++){
  
// //   vatarray.push(this.state.dataitem[i].vatpercnt)
   
// //  console.log("vatpin",vatarray)  
// //  } 
// //  this.setState({vatsarray:vatarray})
// //  console.log("hujh",this.state.vatsarray) 

// //  var res = this.state.vatsarray.map(function(v) {
// //   return parseInt(v, 10);
// // });
// // console.log("gum",res)
// //    addvatarray= 0
// //  for(var i =0 ;i<res.length;i++){
// //   addvatarray += res[i]/100 ;
  
// //  } 
// // console.log("vlfhg",addvatarray)
// //   var vatst= st * addvatarray
// //    console.log("gh",vatst)
// }
  
//  refresh(){


//  }

//   gobackAction=()=>{

//   this.props.navigation.goBack()
//     // this.props.navigation.navigate("newinvoices")
     
// }
// _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
 
// _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

// _handleDatePicked = (date) => {
//  // console.log('A date has been picked: ', date);
//   var dateObj = new Date(date);
//   var momentObj = moment(dateObj);
//   var momentString = momentObj.format("YYYY-MM-DD");
  
//   this.setState({dateset:momentString});
   
//   this._hideDateTimePicker();
// };
// showDateTimePicker = () => this.setState({ DateTimePickerVisible: true });
 
// hideDateTimePicker = () => this.setState({ DateTimePickerVisible: false });

// handleDatePicked = (date1) => {
  

//   var dateObj1 = new Date(date1);
//   var momentObj1 = moment(dateObj1);
//  var momentString1 = momentObj1.format("YYYY-MM-DD");
   
//     if(momentString1>=this.state.dateset){
//   this.setState({datesets:momentString1});
//   this.hideDateTimePicker();
  
// }
// else{
//    alert("Due date must be greater or equal to invoice date")
//   // this.showDateTimePicker();
//  } 
// };
 
// renderSeparator = () => {
//   return (
//     <View
//       style={{
//         height: 1,
//         width:"100%",
//         backgroundColor: "lightgrey",
//         marginTop:0
//       }}
//       />
//   );
// };

// press = (itm,getindex) =>{
//   console.log("pyari",getindex)
    


// // if( getid !== ""){
// //   this.editdraft(vatprsnt,unitprice,quant,discription,Dataobj,this.state.apk,getid)

// // } 
// // else{
// //   this.loginfunction(vatprsnt,unitprice,quant,discription,Dataobj,this.state.apk)
// // }
   
   

//   {Alert.alert('Alert!',
//   'Are you sure you want to delete invoice item ?',
//   [
//     {
//       text: 'No',
     
//       onPress: () => console.log('Cancel Pressed'),
//       style: 'cancel',
//     },
//     {
//       text: 'Yes',
       
//       //onPress: () => {this.deletitemid(vatprsnt,unitprice,quant,discription,Dataobj,this.state.apk,getid,delid,)},
//       onPress: () => {this.deleteItemById(this.state.dataitem,getindex)
         
     
//       },
//     },
//   ],)}
// }
// pres(itm,customer,dateselect,dateselected,objdata,stat,getindex){
//   var delid=itm.ItemID
//   var delid;
//     console.log("objdata",objdata)
//     if(customer == "Select"){
//       alert("Please add customer")
//     }else if(dateselect == "Select Date"){
//       alert("Please select invoice date")
//     }else if(dateselected == "Select Due Date"){
//       alert("Please select invoice due date")
//     }else if(this.state.dataitem == 0){
//       alert("Please add item")
//     }else{
//         if(stat === false){
//          var statusbox = "0"
//         }else{
//           var statusbox = "1"
//         } 
//      var Dataobj = this.state.idcustomer + "|" + dateselected + "|" + dateselect + "|" + statusbox + "|" + "|" + "|" + "1" + "|" + "1"
//      console.log("bigobject",Dataobj )
//      var descobj=[]
//      var hours=[]
//      var unitpr=[]
//      var vati=[]
//      var itemsid=[]
      
//      for(var i = 0;i<objdata.length;i++){
       
//         var descript = objdata[i].Description
//         if(itm.Description ==descript && itm.ItemID==objdata[i].ItemID){ 
//           descript=null 
//           itemsid.push(descript) 
//         }else{ 
//           descobj.push(descript)
//         }    
        
        
//         var hors = objdata[i].Quantity 
//         if(itm.Quantity ==hors && itm.ItemID==objdata[i].ItemID){ 
//           hors=null 
//           hours.push(hors)
//         }else{ 
//           hours.push(hors)
//         }    
        
  
//         var rates = objdata[i].UnitPrice 
//         if(itm.UnitPrice ==rates && itm.ItemID==objdata[i].ItemID){ 
//           rates=null 
//           unitpr.push(rates)
//         }else{ 
//           unitpr.push(rates)
//         }      
       
  
//         var vatses = objdata[i].Tax 
//         if(itm.Tax ==vatses && itm.ItemID==objdata[i].ItemID){ 
//           vatses=null 
//           vati.push(vatses)
//         }else{ 
//           vati.push(vatses)
//         }      
        
  
//         var itemidget= objdata[i].ItemID
//         if(itm.ItemID ==itemidget){
//           console.log(itemidget,"itmid") 
//           itemidget=null
//           console.log(itemidget,"itmid")
//           itemsid.push(itemidget)
         
//         }else{
  
          
//           itemsid.push(itemidget)
//         } 
        
         
//      }
//      var filtered = itemsid.filter(function (el) {
//       return el != null;
//     });
//     var desfilter = descobj.filter(function (el) {
//       return el != null;
//     });
//     var quantfilter = hours.filter(function (el) {
//       return el != null;
//     });
//     var unitfilter = unitpr.filter(function (el) {
//       return el != null;
//     });
//     var vatfilter = vati.filter(function (el) {
//       return el != null;
//     });
    
//      var getid=filtered.join("|") 
//      var discription=desfilter.join("|")
//      var quant = quantfilter.join("|")
//      var unitprice = unitfilter.join("|")
//      var vatprsnt = vatfilter.join("|")
//      this.deletitemid(vatprsnt,unitprice,quant,discription,Dataobj,this.state.apk,getid,delid,)
//   }  
// }
//  deleteItemById =(putindex)=> {
 
//   var dlarray=[]
// this.setState(({ dataitem }) => {
//   const ITEMS = [ ...dataitem ]
//   ITEMS.splice(putindex, 1) 
//   console.log("Item",ITEMS)
//   return { dataitem: ITEMS }
// })
 
 
//  }
// ShowHideTextComponentView = () =>{
 
//   if(this.state.status == true)
//   {
//     this.setState({status: false})
//   }
//   else
//   {
//     this.setState({status: true})
//   }
// }

// getvalue = (allvals) => { 

//   if(allvals == 0){
     
//      return "0.00"
     
//   }else{ 

//     var value = 0;
//     value = parseFloat(allvals).toFixed(2);

//   return value

//   } 
   
// }
// draftfun(customer,dateselect,dateselected,objdata,stat, ){
//    console.log("objdata",objdata)
//   if(customer == "Select"){
//     alert("Please add customer")
//   }else if(dateselect == "Select Date"){
//     alert("Please select invoice date")
//   }else if(dateselected == "Select Due Date"){
//     alert("Please select invoice due date")
//   }else if(this.state.dataitem == 0){
//     alert("Please add item")
//   }else{
//       if(stat === false){
//        var statusbox = "0"
//       }else{
//         var statusbox = "1"
//       } 
//    var Dataobj = this.state.idcustomer + "|" + dateselected + "|" + dateselect + "|" + statusbox + "|" + "|" + "|" + "1" + "|" + "1"
//    console.log("bigobject",Dataobj )
//    var descobj=[]
//    var hours=[]
//    var unitpr=[]
//    var vati=[]
//    var itemsid=[]
    
//    for(var i = 0;i<objdata.length;i++){
     
//       var descript = objdata[i].Description   
//       descobj.push(descript)
      
//       var hors = objdata[i].Quantity   
//       hours.push(hors)

//       var rates = objdata[i].UnitPrice   
//       unitpr.push(rates)

//       var vatses = objdata[i].Tax   
//       vati.push(vatses)

//       var itemidget= objdata[i].ItemID 
//       itemsid.push(itemidget)

       
//    } 
//    var getid=itemsid.join("|")
//    var discription=descobj.join("|")
//    var quant = hours.join("|")
//    var unitprice = unitpr.join("|")
//    var vatprsnt = vati.join("|")
//    Alert.alert("Success","Invoice saved as draft",[{text: 'OK', onPress: () => this.props.navigation.navigate("invoice")}])
    

// if( getid !== ""){
//   this.editdraft(vatprsnt,unitprice,quant,discription,Dataobj,this.state.apk,getid)

// } 
// else{
//   this.loginfunction(vatprsnt,unitprice,quant,discription,Dataobj,this.state.apk)
// }
   
//   } 
   

// }
// draftfun1(customer,dateselect,dateselected,objdata,stat){
   
//   if(customer == "Select"){
//     alert("Please add customer")
//   }else if(dateselect == "Select Date"){
//     alert("Please select invoice date")
//   }else if(dateselected == "Select Due Date"){
//     alert("Please select invoice due date")
//   }else if(this.state.dataitem == 0){
//     alert("Please add item")
//   }else{
//       if(stat === false){
//        var statusbox = "0"
//       }else{
//         var statusbox = "1"
//       }
//    console.log("pintiiii",statusbox)
//    var Dataobj = this.state.idcustomer + "|" + dateselected + "|" + dateselect + "|" + statusbox + "|" + "|" + "|" + "1" + "|" + "2"
//    console.log("bigobject",Dataobj )
//    var descobj=[]
//    var hours=[]
//    var unitpr=[]
//    var vati=[]
   
//    for(var i = 0;i<objdata.length;i++){
     
//       var descript = objdata[i].Description   
//       descobj.push(descript)
      
//       var hors = objdata[i].Quantity   
//       hours.push(hors)

//       var rates = objdata[i].UnitPrice   
//       unitpr.push(rates)

//       var vatses = objdata[i].Tax   
//       vati.push(vatses)

      
    
//    }
   
//    console.log("jksadhh",descobj.join("|"))
//    var discription=descobj.join("|")
//    var quant = hours.join("|")
//    var unitprice = unitpr.join("|")
//    var vatprsnt = vati.join("|")

//     //  var newobject=[]
//     //  newobject.push({Vattipe:vatprsnt,Unitprise:unitprice,quantity:quant,Description:discription,Data:Dataobj,apikeys:this.state.apk,})
//     //  console.log("jaguar",newobject)
//     Alert.alert("Success","Invoice successfully Created",[{text: 'OK', onPress: () => this.props.navigation.navigate("invoice")}])
//    this.loginfunction(vatprsnt,unitprice,quant,discription,Dataobj,this.state.apk)
 

//   } 
   
// }
//   loginfunction(vatprs,unitpr,qua,discrip,Dataob,apks,){
//     console.log("invoceid",this.state.custom)
    
//        var dum = {
//         "API-KEY" : apks,
//         "Data" : Dataob,
//         "Description" : discrip,
//         "Quantity" :qua,
//         "UnitPrice" : unitpr,
//         "Vat" : vatprs
//     } 
//   try {
//      response = fetch(
//      "https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/invoice",
//      {
//        method: "POST",
//        headers: {
//         "Accept": "application/json",
//         "Content-Type": "application/json"
//        },
       
//       body: JSON.stringify(dum)
     
//     }
//    ).then(response => {
//     this.setState({
//        responseAPI:response
//     });
//     })
//    console.log("givedata",this.state.responseAPI) 
    
//     if (this.state.responseAPI.status >= 200 && this.state.responseAPI.status < 300) {
//      const myArrStr = (this.state.responseAPI._bodyInit); 
      
//      const newDatas = JSON.parse(myArrStr)
//      console.log(newDatas,"newdaarr");
    
//     }
//   } catch (errors) {
//    console.log(errors)

//      Alert.alert("Alert","Please Check your internet connection" );
//  }
// }
// editdraft(vatprs,unitpr,qua,discrip,Dataob,apks,getids,){
   
//    var editdraftid=this.state.datarecieves 
  
//     var dum ={
//   "API-KEY":apks,
//   "Data":Dataob,
//   "DelItemId":"",
//   "Description":discrip,
//   "InvoiceID":editdraftid,
//   "ItemID":getids,
//   "Quantity":qua,
//   "UnitPrice":unitpr,
//   "Vat":vatprs
// }
    
// try {
//    response = fetch(
//    "https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/invoice",
//    {
//      method: "Put",
//      headers: {
//       "Accept": "application/json",
//       "Content-Type": "application/json"
//      },
     
//     body: JSON.stringify(dum)
   
//   } 
//  ).then(response => {
//   this.setState({
//      responseAPI:response
//   });
//   })
//  console.log("givedata",this.state.responseAPI) 
//  if (this.state.responseAPI.status >= 200 && this.state.responseAPI.status < 300) {
//   const myArrStr = (this.state.responseAPI.CONTENT); 
   
//   const newDatas = JSON.parse(myArrStr)
//   console.log(newDatas,"newdaarr");
 
//  }
   
// } catch (errors) {
//  console.log(errors)

//    Alert.alert("Alert","Please Check your internet connection" );
// }
// }
// deletitemid(vatprs,unitpr,qua,discrip,Dataob,apks,getids,delids){
//   console.log("delids",delids)
//    var editdraftid=this.state.datarecieves 
  
//     var dum ={
//   "API-KEY":apks,
//   "Data":Dataob,
//   "DelItemId":delids,
//   "Description":discrip,
//   "InvoiceID":editdraftid,
//   "ItemID":getids,
//   "Quantity":qua,
//   "UnitPrice":unitpr,
//   "Vat":vatprs
// }
    
// try {
//    response = fetch(
//    "https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/invoice",
//    {
//      method: "PUT",
//      headers: {
//       "Accept": "application/json",
//       "Content-Type": "application/json"
//      },
     
//     body: JSON.stringify(dum)
   
//   } 
//  ).then(response => {
//   this.setState({
//      responseAPI:response
//   });
//   })
//  console.log("givedata",this.state.responseAPI) 
//  if (this.state.responseAPI.status >= 200 && this.state.responseAPI.status < 300) {
//   const myArrStr = (this.state.responseAPI._bodyInit); 
   
//   const newDatas = JSON.parse(myArrStr)
//   console.log(newDatas,"newdaarr");
 
//  }
   
// } catch (errors) {
//  console.log(errors)

//    Alert.alert("Alert","Please Check your internet connection" );
// }
// }
 
// ontapRow=(item,index,allitem)=>{ 

//   this.props.navigation.navigate("additem",{senditem:item,"endex":index,"reitems":allitem})

// }
// righttap(item,index){
 
//   this.press(item,index) 
// }
// draftfunct(CUSTOM,DATESET,DATESETS,DATAITEM,STATUS){
//   //this.draftfun(this.state.custom,this.state.dateset,this.state.datesets,this.state.dataitem,this.state.status)
 
//   console.log("index",CUSTOM) 
//   console.log("index",DATESET)
//   console.log("index",DATESETS)
//   console.log("index",DATAITEM)
//   console.log("index",STATUS)
   
//   this.draftfun(CUSTOM,DATESET,DATESETS,DATAITEM,STATUS) 
  
//     this.pres(CUSTOM,DATESET,DATESETS,DATAITEM,STATUS)
  
   
// }

//   render() {
//      console.log(this.state.custom, 'mycustomer')
//      console.log(this.state.dataitem, 'dm')
//     return (
//       <SafeAreaView  style={{backgroundColor:"white",flex:1}}>
//        <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"white",marginTop:0}}>
//       <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
//        <TouchableOpacity onPress = {this.gobackAction}  style={{ width:40, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
//        <Image source={require("./Images/leftarrow.png")}  style={{height:20,width:25,tintColor:"white"}}/>
//        </TouchableOpacity>
//        <Text style={{width:dimensions.fullWidth-80,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>NEW INVOICE</Text>
//        </View> 
//        <View style={{width:dimensions.fullWidth,height:(dimensions.fullHeight-160),backgroundColor:"white"}}>
//        <ScrollView>
//        <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:250}}>
//        <TouchableOpacity  onPress={() => this.props.navigation.navigate("selectcustom")} style={{backgroundColor:"#EFEFF4",width:dimensions.fullWidth,height:50,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth-10)/2-5,fontSize:15,fontWeight:"bold",color:"#9A9A9A"}}>Customer</Text><Text style={{width:(dimensions.fullWidth-10)/2-5,fontSize:15,fontWeight:"bold",textAlign:"right",color: this.state.custom === "Select" ?  '#9A9A9A' :  'black'}}>{this.state.custom}</Text> 
//       </TouchableOpacity>
       
       
//        <TouchableOpacity onPress={this._showDateTimePicker}  style={{backgroundColor:"#F3F3F7", width:dimensions.fullWidth,height:50,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth-10)/2-5,fontSize:15,fontWeight:"bold",color:"#9A9A9A"}}>Invoice Date</Text><Text style={{width:(dimensions.fullWidth-10)/2-5,fontSize:15,textAlign:"right",fontWeight:"bold",color: this.state.dateset === "Select Date" ?  '#9A9A9A' :  'black'}}>{this.state.dateset}</Text> 
//        <DateTimePicker
//           isVisible={this.state.isDateTimePickerVisible}
//           onConfirm={this._handleDatePicked}
//           onCancel={this._hideDateTimePicker}
//           minimumDate={new Date()}
//         /></TouchableOpacity>
 
       
//        <TouchableOpacity onPress={this.showDateTimePicker}  style={{backgroundColor:"#EFEFF4",width:dimensions.fullWidth,height:50,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth-10)/2-5,fontSize:15,fontWeight:"bold", color:"#9A9A9A"}}>Due Date</Text><Text style={{width:(dimensions.fullWidth-10)/2-5,fontSize:15,fontWeight:"bold", textAlign:"right",color: this.state.datesets === "Select Due Date" ?  '#9A9A9A' :  'black'}}>{this.state.datesets}</Text>
//        <DateTimePicker
//           isVisible={this.state.DateTimePickerVisible}
//           onConfirm={this.handleDatePicked}
//           onCancel={this.hideDateTimePicker}
//           onDateChange={() => {}}
//         /></TouchableOpacity>
        
//        <View style={{backgroundColor:"#F3F3F7",width:dimensions.fullWidth,height:50,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth-10)/2-5,fontSize:15,opacity:10,fontWeight:"bold", color:"#9A9A9A"}}>Add Bank Details</Text>
//        <View style={{  width:(dimensions.fullWidth-10)/2-5,height:40,alignItems:"flex-end",justifyContent:"center",marginLeft:3}}>
//        <TouchableOpacity onPress = {() => this.ShowHideTextComponentView()} style={{alignItems:"flex-end",width:'20%',height:40,justifyContent:"center",alignItems:"flex-end"}}>
//        {this.state.status === false ? <Image source={require("./Images/blank.png")}  style={{height:20,width:20,tintColor:"#9A9A9A"}}/>:<Image source={require("./Images/check.png")}  style={{height:20,width:20,tintColor:"#9A9A9A"}}/>}
//         </TouchableOpacity></View></View>
        
//        <TouchableOpacity onPress={() => this.props.navigation.navigate("additem")} style={{backgroundColor:"#EFEFF4",width:dimensions.fullWidth,height:50,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><View style={{width:20,height:20,alignItems:"center",justifyContent:"center"}}><Image source={require("./Images/adt.png")}  style={{height:20,width:20,tintColor:"#2685FF"}}/></View><Text style={{width:(dimensions.fullWidth-50),fontSize:15,color:"#2685FF",marginLeft:10,fontWeight:"bold", }}>Add Items</Text></TouchableOpacity>
//        <View style={{width:dimensions.fullWidth,borderBottomColor:"lightgrey",borderBottomWidth:1,marginLeft:0}}></View>
       
//        </View>
        
      
       
//        <View style={{backgroundColor:"red",width:dimensions.fullWidth,}}>
//     <FlatList
//   scrollEnabled={false}
//   data={this.state.dataitem}
//   ItemSeparatorComponent={this.renderSeparator}
//   extraData={this.state}
//   //keyExtractor={(item, index) => item.key}
//   renderItem={({ item ,index}) =>  
  
//   <View>
//         <SwipeRow ref={(c) => { this.component[item,index] = c }}
//                rightOpenValue={-75}
//                onRowOpen={() => {
//                 // if (this.selectedRow && this.selectedRow !== this.component[item,index]) {  this.selectedRow._root.closeRow() }
//                 // this.selectedRow = this.component[item,index]
//                 var useindex=index
//                 if (this.state.selectedRow && this.state.selectedRow != this.component[item,index])
//             {  
//                this.state.selectedRow._root.closeRow();
              
//               } 
//               var countData = this.state.dataitem.length;
//              console.log("count here:",countData);
//              this.state.countData;

//               if (index == countData-1 ){
                
//                 this.state.selectedRow = this.component[item,index-1]
                
//                }
//                else {
                
//                 this.state.selectedRow = this.component[item,index ]
                 
              
//               }
              
               
 
//               }}
//               right={
//                 <TouchableOpacity onPress={() =>  this.righttap(item,this.state.custom,this.state.dateset,this.state.datesets,this.state.dataitem,this.state.status,index+1) } style={{backgroundColor:"#2685FF", flex: 1,
//                 alignItems: 'center',
//                 justifyContent: 'center',
//                 flexDirection: 'column',}}>
//                 <View>
//               <Image style={{height:30,width:30,tintColor:"white"}}  source = {require("./Images/delete.png")}/>
//             </View>
//             </TouchableOpacity>
//               }
//               body={
//                  <TouchableOpacity onPress={() => this.ontapRow(item,index,this.state.dataitem)}  style={styles.flatview}>
//      <View  style={{backgroundColor:"white",width:(dimensions.fullWidth-20),height:25,marginLeft:15,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
//      <Text style={{backgroundColor:"white",width:(dimensions.fullWidth-10)/2-5,fontSize:16,fontWeight:"bold",color:"#9A9A9A"}}>{item.Description}</Text>
//       <Text style={{color:"black",backgroundColor:"white",width:(dimensions.fullWidth-10)/2-5,textAlign:"right",fontSize:16,fontWeight:"bold",color:"black"}}>£ {this.getvalue(item.Quantity * item.UnitPrice)}</Text>
//      </View>
//       </TouchableOpacity>
//               }>
//   </SwipeRow>
//   </View>
//    } 
// />
// </View>
// <View style={{backgroundColor:"#F3F3F7",width:dimensions.fullWidth,height:50,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth)/2-10, fontSize:15,color:"#9A9A9A",fontWeight:"bold"}}>Sub Total</Text><Text numberOfLines={1} style={{width:(dimensions.fullWidth)/2-10,marginLeft:0,fontWeight:"bold", fontSize:16,textAlign:"right",color:"black"}}>£ {this.getvalue(this.state.subtotal)}</Text></View>
// <View style={{backgroundColor:"#EFEFF4",width:dimensions.fullWidth,height:50,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth)/2-10, fontSize:15,color:"#9A9A9A",fontWeight:"bold"}}>VAT</Text><Text numberOfLines={1} style={{width:(dimensions.fullWidth)/2-10,marginLeft:0,fontWeight:"bold", fontSize:16,textAlign:"right",color:"black"}}>£ {this.getvalue(this.state.Vatpercen)}</Text></View>

// <View style={{backgroundColor:"#F3F3F7",width:dimensions.fullWidth,height:50,marginLeft:0,justifyContent:"center",alignItems:"center",flexDirection:"row"}}><Text style={{width:(dimensions.fullWidth)/2-10, fontSize:15,color:"#9A9A9A",fontWeight:"bold"}}>Total</Text><Text numberOfLines={1} style={{width:(dimensions.fullWidth)/2-10,marginLeft:0,fontWeight:"bold", fontSize:16,textAlign:"right",color:"black"}}>£ {this.getvalue(this.state.allamount)} </Text></View>
        

//        </ScrollView>
//       </View> 
//  <View style={{backgroundColor:"white",width:(dimensions.fullWidth),height:80,justifyContent:"center",flexDirection:"row",}}>
  
// <TouchableOpacity onPress={()=>this.draftfunct(this.state.custom,this.state.dateset,this.state.datesets,this.state.dataitem,this.state.status)} style={{width:(dimensions.fullWidth)/2-25,marginTop:5, marginLeft:0,borderRadius:5,height:40,backgroundColor:"#0034A8",alignItems:"center",justifyContent:"center",}}><Text style={{color:"white",fontSize:15,fontWeight:"bold"}}>Draft</Text></TouchableOpacity>
// <TouchableOpacity onPress={()=>this.draftfun1(this.state.custom,this.state.dateset,this.state.datesets,this.state.dataitem,this.state.status)} style={{width:(dimensions.fullWidth)/2-25,marginTop:5,  marginLeft:20,borderRadius:5,height:40,backgroundColor:"#0034A8",justifyContent:"center",alignItems:"center", }}><Text style={{color:"white",fontSize:15,fontWeight:"bold"}}>Create</Text></TouchableOpacity>
// </View> 
//        </View> 
//        <NavigationEvents onDidFocus={() => this.event()} />
//       </SafeAreaView>
//     );
//   }
// }
// const styles = StyleSheet.create({
  
  
//   flatview: {
//      height:30,
//     justifyContent: 'center',
//     paddingTop: 0,
//     borderRadius: 1,
//     alignItems:"center",
//     flexDirection:"row",
//     backgroundColor:"white"
//   },
   
   
   
//   item: {
//     padding: 10,
//     fontSize: 20,
//     height: 45,
//     backgroundColor:"red"
//   }
// });
