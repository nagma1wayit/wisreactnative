import React, { Component } from 'react';
import { View,Modal,StyleSheet,TouchableHighlight,AsyncStorage,FlatList,TextInput,ActivityIndicator,Alert, Text,SafeAreaView ,Dimensions,TouchableOpacity,Image} from 'react-native';

const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }
export default class selectcustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {
    //  isLoading: true,
       text: '' ,
       modalVisible: false,
       Id:"", 
       apk:'',
       name:'',
       address:'',
       dataSource:[],
       array:[],
       newArray:[]
    }
    this.arrayholder = [];
  }
  componentDidMount() {
    AsyncStorage.getItem("apk").then((value) => {
      
      this.setState({apk: value});
      console.log(this.state.apk)
      this.invoices()
    
  }).done();
     
  }
  invoices (){
    //http://docpoke.in/greenleafairupdate/api/getNotifyList?user_id=
    fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/invoiceUserList/' + this.state.apk)
       .then((response) => response.json())
       .then((responseJson) => {
   
         console.log("data invoice:",responseJson[0].UserList)
         
         var arry = responseJson[0].UserList;
        
          
         var nArray = [];
        
         for(var i in arry){ 
            
          nArray.push({names : arry[i],listid:i})

        } 
         console.log('myArray', nArray)
         this.setState({
           //isLoading: false,
           dataSource: nArray
         },
         function() {
                this.arrayholder = nArray;
                console.log("gfg",this.arrayholder)
              }
         );
        
       })
       .catch((error) =>{
         console.log(error);
       });
     }
  
  SearchFilterFunction(text) {
    //passing the inserted text in textinput
    const newData = this.arrayholder.filter(function(item) {
      //applying filter for the inserted text in search bar
      const itemData = item.names ? item.names.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      //setting the filtered newData on datasource
      //After setting the data it will automatically re-render the view
      dataSource: newData,
      text: text,
    });
  }
  ListViewItemSeparator = () => {
    //Item sparator view
    return (
      <View
      style={{
        height: 1,
        width:"100%",
        backgroundColor: "#CED0CE",
        marginTop:0
      }}
      />
    );
  };
  gobackAction=()=>{
 
    // this.state.array.push({"myobj" : "back"})  
    // this.props.navigation.navigate('newinvoices', {"myObj" : this.state.array})      
    this.props.navigation.goBack()
    }
    empty = () => {
      return ( <View style={{height:50,width:dimensions.fullWidth,backgroundColor:"white"}}/>
        );
        
       //
     
    };
    getItem(item1){
      

      AsyncStorage.setItem('custom', JSON.stringify(item1));
      this.setState({custom : item1});
       
     
      // this.state.array.push({"myobj" : "back"})

      // this.props.navigation.navigate('newinvoices', {"myObj" : this.state.array},)    
       
      this.props.navigation.goBack()
      
   
}
getitemnew(nam,adres){


  if(nam==0){
    alert("Please Enter Name")
     
      }
      else if(adres==0){
    alert("Please Enter Address")
      }
      
      else{
        var mArray = [];
 
        mArray.push({names : nam},{addres:adres}) 
        console.log("ng",mArray) 
         //this.setState({newArray:mArray})
         AsyncStorage.setItem('custom',JSON.stringify(mArray));
         this.setState({custom : mArray}); 
         
         this.props.navigation.goBack()
        //  this.state.array.push({"myobj" : "back"})

        //  this.props.navigation.navigate('newinvoices', {"myObj" : this.state.array},)
        // //this.props.navigation.navigate('newinvoices')
        this.setModalVisible(!this.state.modalVisible) 
         
        
  }    
   

 
  
}

setModalVisible(visible) {
  this.setState({modalVisible: visible});
}
 
myValue = (text,nam) => {
  console.log("ad",this.state.address)
  console.log("nm",nam)
  this.setState({address:text})
 
} 

  render() {
    // if (this.state.isLoading) {
    //   //Loading View while data is loading
    //   return (
    //     <View style={{ flex: 1, paddingTop: 0,justifyContent:"center",alignSelf:"center" }}>
    //       <View style={{backgroundColor:"#F3F3F7",borderRadius:10,height:70,width:70,justifyContent:"center",alignItems:"center"}}><ActivityIndicator size="large" color="grey"/></View>
    //     </View>
    //   );
    // }
    
    return (
      <SafeAreaView>
        <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"white",marginTop:0}}>
      <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
       <TouchableOpacity onPress = {this.gobackAction}  style={{ width:40, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
       <Image source={require("./Images/leftarrow.png")}  style={{height:20,width:25,tintColor:"white"}}/>
       </TouchableOpacity>
       {/* <Text style={{width:dimensions.fullWidth-80,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>SELECT CUSTOMER</Text> */}
       <View  style={styles.textInputStyle}>
       <TouchableOpacity   style={{backgroundColor:"white",height:38,width:20,justifyContent:"center",alignItems:"center"}}>
       <Image source={require("./Images/search.png")}  style={{height:15,width:15,tintColor:"lightgrey"}}/>
       </TouchableOpacity>
       <TextInput
          style={{fontSize:16,width:(dimensions.fullWidth-90),paddingLeft:10}}
          onChangeText={text => this.SearchFilterFunction(text)}
          value={this.state.text}
          //underlineColorAndroid="transparent"
          placeholder="Search here"
          
        /></View>
       </View>
      

       <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:dimensions.fullHeight-80}}>
        <FlatList
          data={this.state.dataSource}
          ItemSeparatorComponent={this.ListViewItemSeparator}
          renderItem={({ item, }) => (
            <View style={styles.flatview}>
       <TouchableOpacity onPress={() => this.getItem(item)} disabled={item.listid === "0" ? true : false} style={{backgroundColor:"white",width:(dimensions.fullWidth-10),height:45,flexDirection:"row",alignItems:"center",justifyContent:"center"}}>

       <Text  style={{backgroundColor:"white",paddingLeft:10,fontSize:16,fontWeight:"bold",color:"black"}}>{item.names}</Text>
 
       
       </TouchableOpacity>
  </View>
          )}
         // enableEmptySections={true}
         // style={{ marginTop: 10 }}
          
          ListEmptyComponent={this.empty}
          ListFooterComponent={this.ListViewItemSeparator}
          keyExtractor={(item, index) => index}
        />
        <View style={{backgroundColor:"lightgrey",width:(dimensions.fullWidth),height:80,justifyContent:"center",flexDirection:"row",}}>
        <TouchableOpacity    onPress={() => {this.setModalVisible(true)}}   style={{width:(dimensions.fullWidth),marginTop:5, marginLeft:0,borderRadius:5,height:40,backgroundColor:"lightgrey",alignItems:"center",justifyContent:"center",}}><Text style={{color:"white",fontSize:16,fontWeight:"bold"}}>Add New Customer</Text></TouchableOpacity>
        <Modal
          animationType="none"
          transparent={true}
          visible={this.state.modalVisible}
        
         >
         <View
            style={{flex:1}} 
            activeOpacity={-10}
            //onPressIn={() =>{this.setModalVisible(true)}} 
            onPressOut={() => {this.setModalVisible(false)}}
             
          >
          <View  style={{backgroundColor:"#EFEFF4" ,width:(dimensions.fullWidth-40),marginLeft:20,height:(dimensions.fullHeight-20)/2-80,marginTop:(dimensions.fullHeight-20)/4-20,alignItems:"center"}}>
          <Text style={{fontSize:16,fontWeight:"normal",color:"black",marginTop:5}}>New Customer</Text>
           <TextInput  onChangeText={(text)=> this.setState({name:text})} placeholder={"Enter Name"} style={{width:(dimensions.fullWidth-80),paddingLeft:5,marginTop:40,height:50,borderRadius:8,borderColor:"grey",borderWidth:1,fontSize:16}}/> 
           <TextInput  onChangeText={(text)=>this.setState({address:text})} placeholder={"Enter Address"} style={{width:(dimensions.fullWidth-80),paddingLeft:5,height:50,marginTop:20,borderRadius:8,borderColor:"grey",borderWidth:1,fontSize:16}}/> 
            <TouchableOpacity  onPress={() => this.getitemnew(this.state.name,this.state.address)} style={{backgroundColor:"#2685FF",height:50,marginTop:40,width:(dimensions.fullWidth-120),justifyContent:"center",alignItems:"center"}}>
            <Text style={{fontSize:16,fontWeight:"normal",color:"white"}}>Add Customer</Text>
            </TouchableOpacity>

            </View>
              </View>
        </Modal></View>
      </View>

      </View>
     
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
   
  flatview: {
    height:50,
   justifyContent: 'center',
   paddingTop: 0,
   borderRadius: 1,
   alignItems:"center",
   flexDirection:"row",
   backgroundColor:"white"
 },
  
  
  
 item: {
   padding: 10,
   fontSize: 20,
   height: 45,
   backgroundColor:"white"
 },

  textInputStyle: {
    height: 40,
    width:(dimensions.fullWidth-80),
    marginLeft:10,
    borderWidth: 1,
    marginTop:10,
    paddingLeft: 10,
    justifyContent:"center",
     flexDirection:"row",
    borderColor: 'black',
    backgroundColor: 'white',
  },
});
