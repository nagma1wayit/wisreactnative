import React, { Component } from 'react'
import { Text, View,SafeAreaView,AsyncStorage,navigation,TouchableOpacity,Image,ScrollView,StyleSheet,Dimensions,FlatList} from 'react-native'
import { SwipeRow, Icon, Button } from 'native-base';
 
const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }
  export default class newmileage extends Component {
    constructor(props) {
      super(props);
      this.state = {
       refreshing: false,
       apk:'',
       data1:[]
         
      };
      
    }
    componentDidMount(){


   
      AsyncStorage.getItem("apk").then((value) => {
        
        this.setState({apk: value});
        console.log(this.state.apk)
        this.employee()
      
    }).done();
     }
     employee (){

      fetch('https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/expenseEmployeeList/' + this.state.apk )
         .then((response) => response.json())
         .then((responseJson) => {
    
    
           
           var arr = responseJson[0].Users;
          
           var nArray = [];
            
           for(var i in arr){
              
            nArray.push({names : arr[i]})
          }
           
           this.setState({
            
            data1: nArray
           })
           
           
         })
         .catch((error) =>{
           console.log(error);
         });
       }
    gobackAction=()=>{
       
        this.props.navigation.goBack()
            
             
        }
        empty = () => {
          return (
            <View style={{height:50,width:dimensions.fullWidth,backgroundColor:"#EFEFF4"}}/>
          );
        };

       
        getItem(item3){
            // console.log("itemshjgsfj",item1)




            AsyncStorage.setItem('name3', item3);
            this.setState({name3 : item3});
            
           
            
            this.props.navigation.navigate('newmileages', {
                onGoBack: () => this.refresh()})
            
         
    }
    refresh(){


    }

  render() {
    return (
        <SafeAreaView  style={{backgroundColor:"white",flex:1}}>
        <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"white",marginTop:0}}>
       <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row"}}>
        <TouchableOpacity onPress = {this.gobackAction}  style={{ width:40, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
        <Image source={require("./Images/leftarrow.png")}  style={{height:20,width:25,tintColor:"white"}}/>
        </TouchableOpacity>
        <Text style={{width:dimensions.fullWidth-80,height:30,top:18,marginLeft:10,color:"white",textAlign:"center",fontSize:20,fontWeight:"bold",}}>SELECT EMPLOYEE</Text>
        </View>
        <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:dimensions.fullHeight-80}}>
        
    <FlatList 
  
   ListFooterComponent={this.empty()}
  data={this.state.data1}
  ItemSeparatorComponent={this.renderSeparator}
  renderItem={({item}) => <View style={styles.flatview}>
       <TouchableOpacity onPress={this.getItem.bind(this,item.names)} style={{backgroundColor:"white",width:(dimensions.fullWidth-10),height:50,flexDirection:"row",alignItems:"center"}}>

       <Text  style={{backgroundColor:"white",width:(dimensions.fullWidth-10)/2-5,fontSize:16,fontWeight:"bold",color:"black"}}>{item.names}</Text>

  
  
       
       </TouchableOpacity>
  </View>}
  
  
/>
</View>
        </View>
        </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  
  
    flatview: {
       height:60,
      justifyContent: 'center',
      paddingTop: 0,
      borderRadius: 1,
      alignItems:"center",
      flexDirection:"row",
      backgroundColor:"#EFEFF4"
    },
     
     
     
    itms: {
      padding: 10,
      fontSize: 20,
      height: 45,
      backgroundColor:"red"
    }
  });
