import React, { Component } from 'react';
import { View, Text,SafeAreaView,AsyncStorage,Dimensions,Image,TouchableOpacity,KeyboardAvoidingView,Alert} from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
  
import { Dropdown } from 'react-native-material-dropdown';
const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
  }

export default class homes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      uid:'',
      uemail:'',
      eddata:"",
      apk:'',
      Idsss:"", 
      companycid:'',
      data : [],
      data1 : [],
      yearselected:"",
      idselected:"",
      sharearrayselected:"",
      arraylist : [],
      cldetailsArray:[],
      shareidlist:[],
      arraylist1 : [],
      arraylist2 : []
    };
  }
  componentDidMount = () =>{
    console.disableYellowBox = true
 // console.log(userid)
    // console.log(useremail)
    AsyncStorage.getItem("Idsss").then((vals) => {
      
      this.setState({Idsss: vals});
      console.log(" share deviceId",this.state.Idsss)
      
    
      
    }).done();
    AsyncStorage.getItem("uid").then((value) => {
      
       this.setState({uid: value});
       console.log(this.state.uid)
     
   }).done();
   AsyncStorage.getItem("uemail").then((val) => {
      
    this.setState({uemail: val});
    console.log(this.state.uemail)

  
}).done();

AsyncStorage.getItem("apk").then((value) => {
      
  this.setState({apk: value});
  console.log("hhj",this.state.apk)

}).done();
AsyncStorage.getItem("companycid").then((values) => {
      
  this.setState({companycid: values});
  console.log("companycidss",this.state.companycid)
   
  this.shares()
}).done();


  }
  shares (){
    // //http://docpoke.in/greenleafairupdate/api/getNotifyList?user_id=
    const urlString =  'https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/clientMobileDashShareYear/' + this.state.companycid+'/'+ this.state.Idsss+'/'+ this.state.apk
     console.log("url is here",urlString)
    fetch(urlString)
       .then((response) => response.json())
       .then((responseJson) => {
  
  
       
        console.log("data shares:",responseJson)
        console.log("ist year date:",responseJson[0].year[0].yearname)
        console.log("share holders",responseJson[0].share_holders)

        
        var shareArray =[]
        for (var  i = 0 ; i<responseJson[0].share_holders.length; i++) {
          shareArray.push({value : responseJson[0].share_holders[i].share_holders_name})
          console.log(responseJson[0].share_holders[i].share_holders_name, 'shareholder')
         }
         console.log('this is share array', shareArray)
         this.setState({arraylist1 : shareArray})

        var shareidArray =[]

        for (var  i = 0 ; i<responseJson[0].share_holders.length; i++) {
          shareidArray.push({value :  responseJson[0].share_holders[i].share_holders_id})
          console.log(responseJson[0].share_holders[i].share_holders_id, 'shareid')
         }
         console.log('this is only array', shareidArray)
         this.setState({shareidlist : shareidArray})

          this.setState({idselected:this.state.shareidlist[0].value})
          console.log('this is idselect only object', this.state.idselected)

        
         var yearArray =[]
        for (var  i = 0 ; i<responseJson[0].year.length; i++) {
          yearArray.push({value : responseJson[0].year[i].year})
          console.log(responseJson[0].year[i].year, 'yearssss')
         }
         console.log('this is only array', yearArray)
         this.setState({arraylist2 : yearArray})

          this.setState({yearselected:this.state.arraylist2[this.state.arraylist2.length-1].value})
          console.log('this is only object', this.state.yearselected)

           
         var newArray = [];
         
         for (var  i = 0 ; i<responseJson[0].year.length; i++) {
          newArray.push({value : responseJson[0].year[i].yearname})
          console.log(responseJson[0].year[i].yearname, 'myValue')
         }
         console.log('this is array', newArray)
       
         this.setState({arraylist : newArray})
         this.setState({data : newArray[newArray.length-1].value})

         console.log("listing",this.state.arraylist1)
       if(this.state.arraylist1.length == 0){
         
        this.setState({data1 : "No share holder"})
        
      }else{
        this.setState({data1 : shareArray[0].value}) 
        this.getDatafunction();
      } 

       })
       .catch((error) =>{
         console.log(error);
       });
     }
     onChangeText1(index,value,shareidArray){ 
         var shareArrayid = shareidArray[index].value
       this.setState({idselected:shareArrayid})
       console.log("nagma",this.state.idselected)
       this.getDatafunction();
      
    }

 
  getDatafunction (){
   
    const urlString =  'https://www.wis-accountancy.co.uk/online22jan19/api/Clientapi/clientMobileDashboard/' + this.state.companycid+'/'+ this.state.idselected+'/'+ this.state.yearselected+'/'+ this.state.Idsss+'/'+ this.state.apk
     console.log("url is here",urlString)
    fetch(urlString)
       .then((response) => response.json())
       .then((responseJson) => {
  
  
       
        console.log("getdataapi:",responseJson)
         console.log("client details:",responseJson[0].client_dashboard_details)
        

         this.setState({
          cldetailsArray:responseJson[0].client_dashboard_details
         })
        
       })
       .catch((error) =>{
         console.log(error);
       });
     }
 


  toggleDrawer1 = () => {
 
    console.log(this.props.navigation);
 
    this.props.navigation.toggleDrawer();
 
  }

  onChangeTextPress(index,value,yearArray){ 
   
       var yearonly = yearArray[index].value
     this.setState({yearselected:yearonly})
   
     this.getDatafunction();

  }
  getvalue = (allval) => { 

     if(allval == 0 || allval == null){
        
        return "0.00"
        
     }else{ 

       var value = 0;
       value = parseFloat(allval).toFixed(2);
   
     return value
   
     } 
      
   }
   colorchange = function(allvalue) {
    if(allvalue <0 ){
      
      return {width:'25%',textAlign:"right",color:"red",marginRight:4,fontWeight:"bold"}
   
    }else{
       
      return { width:'25%',textAlign:"right",color:"#128C0D",marginRight:4,fontWeight:"bold"}
    }
     
  }
  render() { 

    return (
      <SafeAreaView style={{backgroundColor:"white",flex:1}}>
      <View style={{width:dimensions.fullWidth,height:dimensions.fullHeight-20,backgroundColor:"pink",marginTop:0}}>
        <View style={{backgroundColor:"#0034A8",width:dimensions.fullWidth,height:60,marginLeft:0,flexDirection:"row",marginTop:0}}>
       <TouchableOpacity onPress={this.toggleDrawer1}  style={{width:35, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0,}}> 
       <Image source={require("./Images/menu.png")}  style={{height:20,width:20}}/>
       </TouchableOpacity>
       <Text style={{ width:dimensions.fullWidth-75 ,top:18,marginLeft:0,color:"white",textAlign:"center",fontSize:18,fontWeight:"bold",}}>SHAREHOLDER DASHBOARD</Text>
       <TouchableOpacity  style={{width:35, height:50,alignItems:"center",justifyContent:"center",top:5,marginLeft:0}}> 
       <Image source={require("./Images/add-button.png")}  style={{height:30,width:30}}/> 
       </TouchableOpacity>
        </View>
        <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:100}}>
        <Text style={{color:"black", width:dimensions.fullWidth-60,marginLeft:30,top:10,textAlign:"center",fontSize:16,fontWeight:"bold"}}>Shareholder Information</Text>
        <View style={{backgroundColor:"white",width:dimensions.fullWidth-10,height:80,marginTop:20,marginLeft:5,justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
        <Dropdown
         data={this.state.arraylist1} //your data collection
         labelFontSize={0}
         pickerStyle={{width:(dimensions.fullWidth-10)/2-20,height:150,marginLeft:(dimensions.fullWidth-10)/3-90,backgroundColor:"white"}}
         baseColor={'#000'}
         onChangeText={(value,index)=>{this.onChangeText1(index, value,this.state.shareidlist)}}
         dropdownOffset={{top:0}}
         containerStyle={{backgroundColor: '#EBEBF1', marginTop:0,marginLeft:0, padding: 0,width:(dimensions.fullWidth-10)/2-20,height:30,}} //replace windowHeight with your own
         value={this.state.data1}
         fontSize={16}
         fontWeight={'normal'}
         rippleOpacity={0}
         style={{ backgroundColor: 'transparent', padding: 0, paddingLeft:3,alignItems:"flex-end" }} //replace size with your own
         />
           <Dropdown
         data={this.state.arraylist}//your data collection
         labelFontSize={0}
         pickerStyle={{width:(dimensions.fullWidth-10)/2-20,height:150,marginLeft:(dimensions.fullWidth-10)/2+18,backgroundColor:"white"}}
         baseColor={'#000'}
         fontSize={16}
         fontWeight={'normal'}
         dropdownOffset={{top:0}}
         containerStyle={{backgroundColor: '#EBEBF1',  marginTop:0,marginLeft:(dimensions.fullWidth-10)/4-50, padding: 0,width:(dimensions.fullWidth-10)/2-20,height:30,}} //replace windowHeight with your own
         value={this.state.data}
         onChangeText={(value,index)=>{this.onChangeTextPress(index, value,this.state.arraylist2)}}
         rippleOpacity={0}
         style={{ backgroundColor: 'transparent', padding: 0, paddingLeft:2 }} //replace size with your own
         />
          
        </View>
        </View>
        

        <View style={{backgroundColor:"white",width:dimensions.fullWidth,height:dimensions.fullHeight - 175,marginTop:0}}>
       

        {this.state.cldetailsArray.layout == 2 ? <ScrollView>
        <KeyboardAvoidingView style={{ flex: 1 }}  keyboardVerticalOffset={160} behavior={"position"}>
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{color:"#9A9A9A",width:'70%',marginLeft:0,fontSize:15,fontWeight:"bold"}}>Higher Rate Tax Threshold</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.higherRateTaxThreshold)}>£ {this.getvalue(this.state.cldetailsArray.higherRateTaxThreshold)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{  width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Tax Free Dividend Allowance</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.taxFreeDividendAllowance)}>£ {this.getvalue(this.state.cldetailsArray.taxFreeDividendAllowance)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{  width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Gross Salary</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.grossSalary)}>£ {this.getvalue(this.state.cldetailsArray.grossSalary)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{  width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Dividends Taken to Date</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.dividendsTakentoDate)}>£ {this.getvalue(this.state.cldetailsArray.dividendsTakentoDate)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{ width:'70%',marginLeft:0,fontWeight:"bold",fontSize:15,color:"#9A9A9A"}}>Total Gross Income to Date</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.totalGrossIncometoDate)}>£ {this.getvalue(this.state.cldetailsArray.totalGrossIncometoDate)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{  width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Basic Dividend Tax at 7.5%</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.basicDividendTax)}>£ {this.getvalue(this.state.cldetailsArray.basicDividendTax)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        <View style={{backgroundColor:"white",width:'100%', height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{ width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Dividends Available(Before higher Dividend Tax)</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.dividendsAvailable)}>£ {this.getvalue(this.state.cldetailsArray.dividendsAvailable)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{  width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Dividends above Threshold</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.dividendsAboveThreshold)}>£ {this.getvalue(this.state.cldetailsArray.dividendsAboveThreshold)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{ width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>High Dividend Tax Implications at 32.5%</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.highDividendTaxImplications)}>£ {this.getvalue(this.state.cldetailsArray.highDividendTaxImplications)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{  width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Total Dividend Tax</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.totalDividendTax)}>£ {this.getvalue(this.state.cldetailsArray.totalDividendTax)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
       
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{ backgroundColor:"white", width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Net Dividend Needed</Text>
        <View style={{backgroundColor:"white",width:'25%',bottom:3,height:35,justifyContent:"center",alignItems:"flex-end"}}><View style={{backgroundColor:"white",width:'90%',height:30,borderWidth:1,borderRadius:3,borderColor:"#9A9A9A",justifyContent:"center",alignItems:"center"}}><TextInput  returnKeyType={'done'} keyboardType={"decimal-pad"} style={{height:40,width:"100%",textAlign:"right",fontWeight:"bold", color:"#128C0D"}}></TextInput>
           </View></View></View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{  width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Dividend Tax Implication</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.higherRateTaxThreshold)}>£ {this.getvalue(this.state.cldetailsArray.higherRateTaxThreshold)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        
        </KeyboardAvoidingView>




        </ScrollView>:<ScrollView>
        <KeyboardAvoidingView style={{ flex: 1 }}  keyboardVerticalOffset={160} behavior={"position"}>
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{color:"#9A9A9A",width:'70%',marginLeft:0,fontSize:15,fontWeight:"bold"}}>Higher Rate Tax Threshold</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.higherRateTaxThreshold)}>£ {this.getvalue(this.state.cldetailsArray.higherRateTaxThreshold)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{  width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Gross Salary</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.grossSalary)}>£ {this.getvalue(this.state.cldetailsArray.grossSalary)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{ width:'70%',marginLeft:0,fontWeight:"bold",fontSize:15,color:"#9A9A9A"}}>Gross Dividends Taken to Date</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.grossDividendsTakentoDate)}>£ {this.getvalue(this.state.cldetailsArray.grossDividendsTakentoDate)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
         
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{ width:'70%',marginLeft:0,fontWeight:"bold",fontSize:15,color:"#9A9A9A"}}>Total Gross Income to Date</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.totalGrossIncometoDate)}>£ {this.getvalue(this.state.cldetailsArray.totalGrossIncometoDate)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>

 
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{ width:'70%',marginLeft:0,fontWeight:"bold",fontSize:15,color:"#9A9A9A"}}>Net Dividends Available(Before)</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.netDividendsAvailable)}>£ {this.getvalue(this.state.cldetailsArray.netDividendsAvailable)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
         
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{ width:'70%',marginLeft:0,fontWeight:"bold",fontSize:15,color:"#9A9A9A"}}>Net Dividends above Threshold</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.netDividendsaboveThreshold)}>£ {this.getvalue(this.state.cldetailsArray.netDividendsaboveThreshold)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>

         
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{ width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>High Dividend Tax Implications at 32.5%</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.highDividendTaxImplications)}>£ {this.getvalue(this.state.cldetailsArray.highDividendTaxImplications)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
         
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{ backgroundColor:"white", width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Net Dividend Needed</Text>
        <View style={{backgroundColor:"white",width:'25%',bottom:3,height:35,justifyContent:"center",alignItems:"flex-end"}}><View style={{backgroundColor:"white",width:'90%',height:30,borderWidth:1,borderRadius:3,borderColor:"#9A9A9A",justifyContent:"center",alignItems:"center"}}><TextInput  returnKeyType={'done'} keyboardType={"decimal-pad"} style={{height:40,width:"100%",textAlign:"right",fontWeight:"bold", color:"#128C0D"}}></TextInput>
           </View></View></View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        <View style={{backgroundColor:"white",width:'100%',height:35,marginLeft:0,alignItems:"center",marginTop:5,justifyContent:"center",flexDirection:"row"}}>
          <Text style={{  width: '70%',marginLeft:4,fontWeight:"bold",color:"#9A9A9A", }}>Dividend Tax Implication</Text>
          <Text style={this.colorchange(this.state.cldetailsArray.higherRateTaxThreshold)}>£ {this.getvalue(this.state.cldetailsArray.higherRateTaxThreshold)}</Text>
          </View>
        <View style={{width:dimensions.fullWidth-10,borderBottomColor:"#9A9A9A",borderBottomWidth:1,marginLeft:5}}></View>
        
        
        </KeyboardAvoidingView>
        </ScrollView>}
      
        <View style={{backgroundColor:"#EBEBF1",width:dimensions.fullWidth,marginLeft:0,height:100,alignItems:"center"}}>
<Text style={{width:(dimensions.fullWidth-16),fontSize:14,marginLeft:10,marginTop:0, }}>Disclaimer: The calculations are purely based on PAYE and Dividend income from UK (except Scotland)and does not support Scottish tax rates,additional incomes,income over £100,000.00. </Text>
</View>
 
        </View>
        
 
        </View>
      </SafeAreaView>
    );
  }
}