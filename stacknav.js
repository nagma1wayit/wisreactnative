import React, { Component } from 'react';
import { View,AsyncStorage, Text,SafeAreaView ,Dimensions,ScrollView,Image,Alert,TouchableOpacity,StyleSheet} from 'react-native';
const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}
export default class  stacknav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status : false,
      uname:'',
      vno:'',
      compregno:'',
      uid:''
    };
  }
  componentDidMount = () =>{
    console.disableYellowBox = true
    
    // AsyncStorage.setItem("uid","");

    AsyncStorage.getItem("uname").then((value) => {
      
       this.setState({uname: value});
       console.log(this.state.uname)
     
   }).done();
   AsyncStorage.getItem("vno").then((val) => {
      
    this.setState({vno: val});
    console.log(this.state.vno)

  
}).done();
AsyncStorage.getItem("compregno").then((vals) => {
      
  this.setState({compregno: vals});
  console.log(this.state.compregno)


}).done();


  }
  ShowHideTextComponentView = () =>{
 
    if(this.state.status == true)
    {
      this.setState({status: false})
    }
    else
    {
      this.setState({status: true})
    }
  }
  Logoutbutton = () =>{
    AsyncStorage.setItem("uid","");
    Alert.alert('Logout',
      'Are you sure you want to logout?',
      [
        {
          text: 'Cancel',
         
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'ok',
          
          onPress: () =>  this.props.navigation.closeDrawer(this.props.navigation.navigate("Home"),this.state.uid),

          //this.props.navigation.navigate("Home"),
        },
      ],)
  }

  render() {
    return (
      <SafeAreaView style={{flex:1,backgroundColor:"white"}}>
      <View style={{width:dimensions.fullWidth-75,height:dimensions.fullHeight-20,backgroundColor:"white"}}>
      
      <View style={{width:dimensions.fullWidth-75,height:"30%",backgroundColor:'#0034A8',flexDirection:"row"}}>
      <View style={{height:"100%",left:2 ,  width:dimensions.fullWidth-85,flexDirection:"row"}}>
      <View style={{width:'80%',height:'100%'}}>
      <Text style = {styles.containText} >Mr. {this.state.uname}</Text>
    <Text style = {styles.containTextMeduim} >{this.state.compregno}</Text>
    <Text style = {styles.containTextSmall} >Company Reg. no.</Text>
    <Text style = {styles.containTextMeduim} >{this.state.vno}</Text>
    <Text style = {styles.containTextSmall} >VAT Reg. no. </Text>
      {/* <Text style={{ width:dimensions.fullWidth-150,height:35, marginLeft:10,fontSize:25,fontWeight:'600',justifyContent:"center",top:15,color:"white"}}>Nagma Salaria</Text>
      <TouchableOpacity style={{ width:(dimensions.fullWidth-75)/6,height:35, marginLeft:5,marginTop:15,alignItems:"center",justifyContent:"center"}}>
      <Image source={require("./Images/logout.png")}  style={{height:25,width:25}}></Image></TouchableOpacity> */}
      </View>
      <View style={{width:'20%',height:'100%',alignItems:"flex-end"}}>
      <TouchableOpacity  onPress={() => this.Logoutbutton()} style={{width:'50%',height:40}}><Image
        source = {require('./Images/logout.png')}
        style = {styles.imgStyle}

        /></TouchableOpacity></View>
       
      </View> 
      </View>
      <View style={{width:dimensions.fullWidth-75,height:"70%",backgroundColor:"white",marginTop:0}}>
      <ScrollView style={{backgroundColor:"white"}}>
        
       <TouchableOpacity  onPress={() => this.ShowHideTextComponentView()} style={{ width:dimensions.fullWidth-85,height:45,marginLeft:5,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Image source={require("./Images/dashboard.png")}  style={{height:20,width:20,marginLeft:1}}></Image>
       <Text style={{width:dimensions.fullWidth-150,height:20,fontSize:18,fontWeight:"normal",marginLeft:10}}>Dashboard</Text>
        
       {this.state.status === false ? <Image source={require("./Images/arrDown.png")}  style={{height:15,width:15,marginLeft:10}}></Image>:<Image source={require("./Images/arrUp.png")}  style={{height:15,width:15,marginLeft:10}}></Image>}
       </TouchableOpacity>
   {this.state.status === true ? <View style={{ width:dimensions.fullWidth-85,height:80,marginLeft:5,marginTop:5}}> 
       <TouchableOpacity onPress={() => this.props.navigation.closeDrawer(this.props.navigation.navigate("companyscreen"),this.ShowHideTextComponentView())}style={{ width:dimensions.fullWidth-85,height:40,marginLeft:0,marginTop:0,justifyContent:"center"}}>
        <Text style={{width:dimensions.fullWidth-150,height:20,fontSize:15,fontWeight:"normal",marginLeft:30}}>Company</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.props.navigation.closeDrawer(this.props.navigation.navigate("shares"),this.ShowHideTextComponentView())} style={{ width:dimensions.fullWidth-85,height:40,marginLeft:0,marginTop:5,justifyContent:"center" }}>
        <Text style={{width:dimensions.fullWidth-150,height:20,fontSize:15,fontWeight:"normal",marginLeft:30}}>Shareholders</Text>
        </TouchableOpacity>
       </View> :null}
       
       <TouchableOpacity  onPress={() =>  this.props.navigation.closeDrawer(this.props.navigation.navigate("invoice"))} style={{ width:dimensions.fullWidth-85,height:45,marginLeft:5,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Image source={require("./Images/Invoice.png")}  style={{height:20,width:20,marginLeft:1}}></Image>
       <Text style={{width:dimensions.fullWidth-150,height:20,fontSize:18,fontWeight:"normal",marginLeft:10}}>Invoices</Text>
       
       </TouchableOpacity>
       
       <TouchableOpacity onPress={() =>  this.props.navigation.closeDrawer(this.props.navigation.navigate("expen"))} style={{ width:dimensions.fullWidth-85,height:45,marginLeft:5,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Image source={require("./Images/expense.png")}  style={{height:20,width:20,marginLeft:1}}></Image>
       <Text style={{width:dimensions.fullWidth-150,height:25,fontSize:18,fontWeight:"normal",marginLeft:10}}>Expenses</Text>
        
       </TouchableOpacity>
       
       <TouchableOpacity onPress={() =>  this.props.navigation.closeDrawer(this.props.navigation.navigate("milen"))}  style={{ width:dimensions.fullWidth-85,height:45,marginLeft:5,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Image source={require("./Images/milega.png")}  style={{height:20,width:20,marginLeft:1}}></Image>
       <Text style={{width:dimensions.fullWidth-150,height:25,fontSize:18,fontWeight:"normal",marginLeft:10}}>Mileage</Text>
        
       </TouchableOpacity>
       
       <TouchableOpacity onPress={() =>  this.props.navigation.closeDrawer(this.props.navigation.navigate("notification"))}  style={{width:dimensions.fullWidth-85,height:45,marginLeft:5,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Image source={require("./Images/notification.png")}  style={{height:20,width:20,marginLeft:1}}></Image>
       <Text style={{width:dimensions.fullWidth-150,height:20,fontSize:18,fontWeight:"normal",marginLeft:10}}>Notifications</Text>
      
       </TouchableOpacity>
       
       <TouchableOpacity onPress={() =>  this.props.navigation.closeDrawer(this.props.navigation.navigate("refer"))} style={{ width:dimensions.fullWidth-85,height:45,marginLeft:5,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Image source={require("./Images/referfrnd.png")}  style={{height:20,width:20,marginLeft:1}}></Image>
       <Text style={{width:dimensions.fullWidth-150,height:20,fontSize:18,fontWeight:"normal",marginLeft:10}}>Refer a Friend</Text>
       
       </TouchableOpacity>
       
       <TouchableOpacity onPress={() =>  this.props.navigation.closeDrawer(this.props.navigation.navigate("cont"))} style={{ width:dimensions.fullWidth-85,height:45,marginLeft:5,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Image source={require("./Images/contact.png")}  style={{height:20,width:20,marginLeft:1}}></Image>
       <Text style={{width:dimensions.fullWidth-150,height:20,fontSize:18,fontWeight:"normal",marginLeft:10}}>Contact Us</Text>
       
       </TouchableOpacity>
       
       <TouchableOpacity onPress={() =>  this.props.navigation.closeDrawer(this.props.navigation.navigate("abt"))} style={{width:dimensions.fullWidth-85,height:45,marginLeft:5,marginTop:10,flexDirection:"row",alignItems:"center"}}>
       <Image source={require("./Images/about.png")}  style={{height:20,width:20,marginLeft:1}}></Image>
       <Text style={{ width:dimensions.fullWidth-150,height:20,fontSize:18,fontWeight:"normal",marginLeft:10}}>About Us</Text>
        
       </TouchableOpacity>
       

      </ScrollView>
      </View>
        
      </View>
        
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor:'#0034A8' ,
    height:200,
    flexDirection: 'row',
    alignItems: 'flex-start',

  },
  imgStyle:{
    position: "absolute",
    top: 10,
    right: 0 ,
    width:25,
    height:25,
    tintColor:"#FF9F34"
  },
  containText :{
    top: 10,
    flex: 1,
    textAlign: 'left',
    fontSize: 24,
    color: '#FF9F34',
  },
  containTextSmall :{
    top: 10,
    flex: 1,
    fontSize: 15,
    color: '#FF9F34',
    textAlign: 'left',
    left: 0

  },
  containTextMeduim :{
    top: 20,
    flex: 1,
    textAlign: 'left',
    fontSize: 20,
    color: '#FF9F34',
    left: 0,
  },

});